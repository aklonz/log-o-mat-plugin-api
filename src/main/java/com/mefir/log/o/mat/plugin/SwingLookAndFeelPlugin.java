package com.mefir.log.o.mat.plugin;

import java.util.List;

public interface SwingLookAndFeelPlugin extends Plugin<SwingLookAndFeelPlugin> {

	List<SwingLookAndFeelContainer> getLookAndFeels();

}
