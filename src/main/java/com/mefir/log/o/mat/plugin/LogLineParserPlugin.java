package com.mefir.log.o.mat.plugin;

import java.util.List;

public interface LogLineParserPlugin extends Plugin<LogLineParserPlugin> {

	List<LogLineParser> getLogLineParsers();

}

