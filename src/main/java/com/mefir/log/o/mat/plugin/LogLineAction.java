package com.mefir.log.o.mat.plugin;

import javax.swing.Icon;

public interface LogLineAction {

	/**
	 * @return	title of this {@link LogLineAction } implementation.
	 */
	String title();

	/**
	 * @return	optional icon of this {@link LogLineAction } implementation.
	 */
	default Icon icon() {
		return null;
	}

	boolean matches(LogLineContext lineContext);

	void run(LogLineContext lineContext) throws Exception;

}
