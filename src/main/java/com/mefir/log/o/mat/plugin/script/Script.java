package com.mefir.log.o.mat.plugin.script;

import java.io.Closeable;
import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.util.LinkedList;

import com.mefir.log.o.mat.plugin.Plugin;

public abstract class Script {

	private final Object lock = new Object();
	private ClassLoader classLoader;
	private File directory;
	private String directoryString;
	private File file;
	private String fileString;
	private URL url;
	private String urlString;
	private ScriptUtilities util;
	private String script;
	private boolean initialized = false;
	private boolean valid = false;
	private Throwable initException = null;
	private Plugin<?> plugin;
	private boolean autoCloseEnabled = false;
	private LinkedList<Closeable> autoCloseResources = null;


	protected Script(ClassLoader classLoader, File directory, String script) {
		this.classLoader = classLoader;
		this.directory = directory;
		this.directoryString = directory.getAbsolutePath();
		try { this.directoryString = directory.getCanonicalPath(); } catch(Exception | AssertionError ex) { /** ignore */ }
		this.script = script;
		this.util = new ScriptUtilities(this);
	}

	protected Script(File directory, String script) {
		this(Thread.currentThread().getContextClassLoader(), directory, script);
	}

	protected abstract boolean internalInit() throws Exception;

	protected abstract Object internalInvokeFunction(ScriptFunction function, Object ... arguments) throws Exception;


	public final File directory() {
		return this.directory;
	}

	protected final String directoryString() {
		return this.directoryString;
	}

	public final File file() {
		return this.file;
	}

	protected final String fileString() {
		return this.fileString;
	}

	final void file(File file) {
		if(file!=null) {
			this.file = file;
			this.fileString = file.getAbsolutePath();
			try { this.fileString = file.getCanonicalPath(); } catch(Exception | AssertionError ex) { /** ignore */ }
		} else {
			this.file = null;
			this.fileString = null;
		}
	}

	public final URL url() {
		return this.url;
	}

	protected final String urlString() {
		return this.urlString;
	}

	final void url(URL url) {
		if(url!=null) {
			this.url = url;
			this.urlString = url.toString();
		} else {
			this.url = null;
			this.urlString = null;
		}
	}

	protected final String script() {
		return this.script;
	}

	protected final ClassLoader classLoader() {
		return this.classLoader;
	}

	public final ScriptUtilities utilities() {
		return this.util;
	}

	protected void dispose() {
		ScriptUtilities u;

		synchronized (this.lock) {
			this.initialized = true;
			this.valid = false;
			this.classLoader = null;
			this.directory = null;
			this.script = null;
			this.initException = null;
			u = this.util;
			this.util = null;
		}

		if(u!=null) {
			u.dispose();
		}
	}

	final boolean valid() {
		synchronized (this.lock) {
			if(!this.initialized) {
				try {
					this.init();
				} catch(Exception | AssertionError ex) {
					//do not throw an invalid-exception for the valid check
				}
			}
			return this.valid;
		}
	}

	final void init() {
		synchronized (this.lock) {
			if(!this.initialized) {
				this.initialized = true;
				try {
					this.valid = this.internalInit();
				} catch(Exception | AssertionError ex) {
					this.initException = ex;
					this.valid = false;
				}
			}

			if(!this.valid) {
				final Throwable th = this.initException;
				if(th!=null) {
					throw th instanceof RuntimeException ? ((RuntimeException) th) : new UnsupportedOperationException(th);
				}
				throw new NullPointerException("No compatible script engine available.");
			}
		}
	}

	final Object invokeFunction(ScriptFunction function, Object ... arguments) {
		try {
			synchronized (this.lock) {
				try {
					this.autoCloseEnabled = true;
					return this.internalInvokeFunction(function, arguments);
				} finally {
					this.autoCloseEnabled = false;
					final LinkedList<Closeable> list = this.autoCloseResources;
					this.autoCloseResources = null;
					if(list!=null) {
						for(final Closeable resource:list) {
							try { resource.close(); } catch(Exception | AssertionError ex) { this.error(ex); }
						}
					}
				}
			}
		} catch(RuntimeException | AssertionError ex) {
			this.error(ex);
			throw ex;
		} catch(Exception ex) {
			this.error(ex);
			throw new UnsupportedOperationException(ex);
		}
	}

	void registerForAutoClose(final Closeable resource) {
		synchronized(this.lock) {
			if(this.autoCloseEnabled) {
				if(this.autoCloseResources==null) {
					this.autoCloseResources = new LinkedList<>();
				}
				//add first so the resource will be closed also first before older resources
				//(To ensure a PrintStream or buffered stream will be close before the underlying output stream.)
				this.autoCloseResources.addFirst(resource);
			}
		}
	}

	public final void error(Throwable th) {
		PrintStream err = null;
		final ScriptUtilities u = this.util;

		if(u!=null) {
			err = u.err!=null ? u.err : u.out;
		}

		err = err!=null ? err : System.err;

		th.printStackTrace(err);
	}

	protected final Plugin<?> plugin() {
		synchronized (this.lock) {
			return this.plugin;
		}
	}

	final void plugin(Plugin<?> plugin) {
		synchronized (this.lock) {
			if(this.plugin==null) {
				this.plugin = plugin;
			}
		}
	}

}
