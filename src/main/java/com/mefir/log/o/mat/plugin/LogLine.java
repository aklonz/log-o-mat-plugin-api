package com.mefir.log.o.mat.plugin;

import java.time.ZonedDateTime;

public interface LogLine {

	/**
	 * @return raw bytes of the line including the line end marker
	 */
	byte[] bytes();

	/**
	 * @return	line end marker, if available; otherwise an empty string will be returned
	 */
	String endOfLine();

	/**
	 * @return	the line as string without the line end marker
	 */
	String string();

	/**
	 * @return A {@link ZonedDateTime} value representing time and date this line was written to the file.
	 *         Can be <code>null</code> if the timestamp format of the line is not recognized.
	 */
	ZonedDateTime timestamp();

	long lineNumber();

	long lineInFile();

	long globalLineNumber();

}
