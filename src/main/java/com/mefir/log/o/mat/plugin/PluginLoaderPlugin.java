package com.mefir.log.o.mat.plugin;

import java.io.File;
import java.util.List;

/**
 * Plugin that loads other plugins
 */
public interface PluginLoaderPlugin extends Plugin<PluginLoaderPlugin> {

	<PLUGIN extends Plugin<?>> List<PluginLoaderLoadedPlugin<PLUGIN>> loadPlugins(ClassLoader classLoader, File directory, Class<PLUGIN> pluginClass);

}
