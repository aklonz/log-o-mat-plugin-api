package com.mefir.log.o.mat.plugin;

import java.nio.charset.Charset;
import java.time.ZonedDateTime;

public interface FileInfo {

	/**
	 * @return	the name of the file
	 */
	String name();

	/**
	 * @return	short name/description of the file
	 */
	String shortName();

	/**
	 * @return	the charset of the file
	 */
	Charset charset();

	/**
	 * @return the length in bytes.
	 */
	long byteLength();

	/**
	 * @return	number of read/processed bytes in this file
	 */
	long bytesRead();

	/**
	 * @return A {@link ZonedDateTime} value representing the date and time the file was modified.
	 */
	ZonedDateTime lastModified();

	/**
	 * @return String of the first 1024 bytes of the file or the complete file if shorter than 1024 byte. The actual string length depends on {@link #charset()}
	 */
	String header();

	/**
	 * @return	number of lines in this file
	 */
	long length();

	/**
	 * @return Global line number at which the file starts. For single file logs this will always be 0.
	 */
	long lineOffset();

	/**
	 * Stores reusable plugin data for this file.
	 * The data will be kept in short time memory while the plugin is processing the file and while enough memory is available.
	 * Stored data can be accessed with {@link #get(Object)}
	 * @param key	unique key to identify the data
	 * @param data	data to store
	 */
	void put(String key, Object data);

	/**
	 * Gets data, previously created by this plugin and stored with {@link #put(String, Object)}.
	 * It is not guaranteed for previously created data to be still available.
	 * Data will only be kept in short time memory and only while the plugin is processing the file and while enough memory is available
	 * @param key	unique key to identify the data
	 * @return		the requested data if available; <code>null</code> otherwise
	 */
	Object get(Object key);

}
