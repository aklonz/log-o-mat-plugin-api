package com.mefir.log.o.mat.plugin.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;

import com.mefir.log.o.mat.plugin.ArchiveEntry;
import com.mefir.log.o.mat.plugin.ArchiveExtractContext;
import com.mefir.log.o.mat.plugin.ArchiveExtractor;
import com.mefir.log.o.mat.plugin.ArchivePlugin;
import com.mefir.log.o.mat.plugin.PluginContext;
import com.mefir.log.o.mat.plugin.PluginInfo;

public class TestExtractorPlugin implements ArchivePlugin {


	@Override
	public List<ArchiveExtractor> getArchiveExtractors() {
		return Collections.singletonList(new TestExtractor());
	}

	@Override
	public PluginInfo info() {
		return null;
	}

	@Override
	public boolean init(final PluginContext context) {
		return true;
	}

	@Override
	public void dispose() {

	}

	private class TestExtractor implements ArchiveExtractor {

		@Override
		public String title() {
			return "test extractor";
		}

		@Override
		public List<String> fileExtensions() {
			return Collections.singletonList(".txt");
		}

		@Override
		public boolean isSupported(final File archive) {
			final boolean supported = "x.txt".equalsIgnoreCase(archive.getName());
			return supported;
		}

		@Override
		public List<ArchiveEntry> list(final File archive) {
			final ArchiveEntry entry = new ArchiveEntry() {
				@Override
				public ZonedDateTime lastModified() {
					return Instant.ofEpochMilli(archive.lastModified()).atZone(ZoneId.systemDefault());
				}

				@Override
				public Long byteLength() {
					return Long.valueOf(archive.length());
				}

				@Override
				public String name() {
					return archive.getName();
				}
			};
			return Collections.singletonList(entry);
		}

		@Override
		public void extract(final ArchiveExtractContext context, final File archive, final ArchiveEntry entry, final OutputStream target) throws IOException {
			try (FileInputStream in = new FileInputStream(archive)) {
				int b = in.read();
				while(b>=0) {
					try { Thread.sleep(5); } catch(final Exception ex) { /** ignore */ }
					target.write(b);
					b = in.read();
				}
			}
		}
	}
}
