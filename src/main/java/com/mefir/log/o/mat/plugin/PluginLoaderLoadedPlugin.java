package com.mefir.log.o.mat.plugin;

import java.io.File;
import java.net.URL;

public final class PluginLoaderLoadedPlugin<PLUGIN extends Plugin<?>> {

	private final PLUGIN plugin;
	private final File file;
	private final URL url;

	public PluginLoaderLoadedPlugin(PLUGIN plugin, File file, URL url) {
		if(plugin==null) {
			throw new NullPointerException("Plugin must not be null.");
		}
		if((file==null) && (url==null)) {
			throw new NullPointerException("Either file or URL has to be provided.");
		}
		this.plugin = plugin;
		this.file   = file;
		this.url    = url;
	}

	public final PLUGIN plugin() {
		return this.plugin;
	}

	public final File file() {
		return this.file;
	}

	public final URL url() {
		return this.url;
	}

}

