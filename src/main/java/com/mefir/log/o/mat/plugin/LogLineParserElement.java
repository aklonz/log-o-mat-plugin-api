package com.mefir.log.o.mat.plugin;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class LogLineParserElement {

	protected final Object lock = new Object();
	private final String name;
	private final LinkedHashMap<LogLineParserColumn, String> values = new LinkedHashMap<>();
	private LogLineParserNode parent;

	LogLineParserElement(String name) {
		this.name = name;
	}

	/**
	 * @return	title of this element
	 */
	public final String name() {
		return this.name;
	}

	/**
	 * @return	column values of this element
	 */
	public final List<String> values() {
		synchronized (this.lock) {
			return new ArrayList<>(this.values.values());
		}
	}

	/**
	 * @return	column definitions of this element
	 */
	public final List<LogLineParserColumn> columns() {
		synchronized (this.lock) {
			return new ArrayList<>(this.values.keySet());
		}
	}

	/**
	 * Gets the value for the given column.
	 * @param column	The column to get the value for. Must not be <code>null</code>.
	 * @return	The value for the given column of this element. Can be <code>null</code>.
	 */
	public final String get(final LogLineParserColumn column) {
		if(column==null) {
			throw new NullPointerException("Column must not be null.");
		}
		synchronized (this.lock) {
			return this.values.get(column);
		}
	}

	/**
	 * Gets the value for the column with the given {@link LogLineParserColumn#index()}.
	 * @param column	{@link LogLineParserColumn#index()} of the column to get the value for.
	 * @return	The value for the given column of this element. Can be <code>null</code>.
	 */
	public final String get(int column) {
		synchronized (this.lock) {
			for(final Map.Entry<LogLineParserColumn, String> entry:this.values.entrySet()) {
				final LogLineParserColumn c = entry.getKey();
				if((c!=null) && (c.index()==column)) {
					return entry.getValue();
				}
			}
		}
		return null;
	}

	/**
	 * Sets the value for the given column.
	 * @param column	Column to set the value for. Must not be <code>null</code>.
	 * @param value		Value to set. Can be <code>null</code>.
	 * @return	The old value for the given column. Can be <code>null</code>.
	 */
	public final String put(final LogLineParserColumn column, final String value) {
		if(column==null) {
			throw new NullPointerException("Column must not be null.");
		}
		synchronized (this.lock) {
			return this.values.put(column, value);
		}
	}

	/**
	 * Sets the value for the given column.
	 * @param column	{@link LogLineParserColumn#index()} of the Column to set the value for.
	 * 					If no column with the given index exits, it will automatically be created.
	 * @param value		Value to set. Can be <code>null</code>.
	 * @return	The old value for the given column. Can be <code>null</code>.
	 */
	public final String put(int column, final String value) {
		synchronized (this.lock) {
			for(final LogLineParserColumn c:this.values.keySet()) {
				if((c!=null) && (c.index()==column)) {
					return this.put(c, value);
				}
			}
			return this.put(LogLineParserColumn.dummyColumn(column), value);
		}
	}

	final LogLineParserNode parent() {
		synchronized (this.lock) {
			return this.parent;
		}
	}

	final void parent(final LogLineParserNode parent) {
		synchronized (this.lock) {
			if(parent==null) {
				throw new NullPointerException("Parent must not be null.");
			}
			if(this.parent!=null) {
				throw new UnsupportedOperationException("Node '" + this.name + "' is already linked to parent node '" + parent.name() + "'.");
			}
			this.parent = parent;
		}
	}

	@Override
	public abstract String toString();
}
