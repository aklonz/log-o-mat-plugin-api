package com.mefir.log.o.mat.plugin;

import javax.swing.Icon;

public interface LogAction {

	/**
	 * @return	title of this {@link LogAction } implementation.
	 */
	String title();

	/**
	 * @return	optional icon of this {@link LogAction } implementation.
	 */
	default Icon icon() {
		return null;
	}

	boolean matches(LogActionContext context);


	/**
	 * @param context	Context of the log to process.
	 * 					For swing applications the context will be an instance of {@link LogSwingActionContext}.
	 */
	void run(LogActionContext context) throws Exception;


}
