package com.mefir.log.o.mat.plugin;

public interface ArchiveExtractContext extends Context {

	/**
	 * @return	<code>true</code> if the extract job was cancelled by the main application
	 */
	boolean cancelled();


	/**
	 * Callback to update the extract progress
	 * @param progress	current progress a double between 0 and 1.
	 */
	void progress(double progress);


}
