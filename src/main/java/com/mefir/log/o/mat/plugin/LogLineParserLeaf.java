package com.mefir.log.o.mat.plugin;

/**
 * Tree leaf created by a {@link LogLineParser}.
 */
public final class LogLineParserLeaf extends LogLineParserElement {

	/**
	 * Creates an instance with the given name.
	 * @param name	Title of this leaf
	 */
	public LogLineParserLeaf(final String name) {
		super(name);
	}

	@Override
	public String toString() {
		return this.name() + " = " + this.values();
	}
}
