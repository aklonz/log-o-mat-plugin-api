package com.mefir.log.o.mat.plugin;

public interface SwingLookAndFeelContainer {

	/**
	 * @return	Unique key of this LookAndFeel
	 */
	String key();

	/**
	 * @return	Title for this LookAndFeel to show in the UI configuration.
	 */
	String title();

	/**
	 *
	 * @return	true if this LookAndFeel should be registered as default LookAndFeel.
	 * 			If multiple LookAndFeels are marked as default,
	 * 			only the first LookAndFeel will be registered as default LookAndFeel.
	 */
	default boolean isDefaultLookAndFeel() {
		return false;
	}

	/**
	 * installs the LookAndFeel
	 */
	void install() throws Exception;

}
