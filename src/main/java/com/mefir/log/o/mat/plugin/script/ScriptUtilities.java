package com.mefir.log.o.mat.plugin.script;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import com.mefir.log.o.mat.plugin.PatternMatch;
import com.mefir.log.o.mat.plugin.PatternParameter;
import com.mefir.log.o.mat.plugin.PluginInfo;
import com.mefir.log.o.mat.plugin.PluginInfoBuilder;

public class ScriptUtilities {

	private Script parent;
	public PrintStream out = System.out;
	public PrintStream err = System.err;


	ScriptUtilities(Script parent) {
		this.parent = parent;
	}

	void dispose() {
		this.parent = null;
	}

	private void registerForAutoClose(final Closeable resource) {
		Script p = parent;
		if(p!=null) {
			p.registerForAutoClose(resource);
		}
	}

	/**
	 * @return	the working directory of the script
	 */
	public String directory() {
		return this.parent.directoryString();
	}

	public String file() {
		return this.parent.fileString();
	}

	public String url() {
		return this.parent.urlString();
	}

	/**
	 * Creates a java instance of the given java class.
	 * @param className	name of the java class to create an instance of.
	 * @return the created instance
	 */
	@SuppressWarnings("rawtypes")
	public Object newInstance(Object className) {
		String classNameString = this.toString(className);
		if(classNameString==null) {
			throw new NullPointerException("ClassName must not be null.");
		}
		classNameString = classNameString.trim();
		if(classNameString.isEmpty()) {
			throw new IllegalArgumentException("ClassName must not be empty.");
		}
		switch (classNameString.toLowerCase()) {
			case "map":
			case "java.util.map":
			case "hashmap":
			case "java.util.hashmap":
				return new HashMap();
			case "collection":
			case "java.util.collection":
			case "list":
			case "java.util.list":
			case "linkedlist":
			case "java.util.linkedlist":
				return new LinkedList();
			case "arraylist":
			case "java.util.arraylist":
				return new ArrayList();
			case "set":
			case "java.util.set":
			case "hashset":
			case "java.util.hashset":
				return new HashSet();
			case "linkedhashset":
			case "java.util.linkedhashset":
				return new LinkedHashSet();
			case "linkedhashmap":
			case "java.util.linkedhashmap":
				return new LinkedHashMap();
			case "treemap":
			case "java.util.treemap":
				return new TreeMap();
			case "treeset":
			case "java.util.treeset":
				return new TreeSet();
			case "atomicreference":
			case "java.util.concurrent.atomic.atomicreference":
				return new AtomicReference();
			case "atomicinteger":
			case "java.util.concurrent.atomic.atomicinteger":
				return new AtomicInteger();
			case "atomiclong":
			case "java.util.concurrent.atomic.atomiclong":
				return new AtomicLong();
			case "atomicboolean":
			case "java.util.concurrent.atomic.atomicboolean":
				return new AtomicBoolean();
			case "stringbuilder":
			case "java.lang.stringbuilder":
				return new StringBuilder();
			case "hashtable":
			case "java.util.hashtable":
			case "dictionary":
			case "java.util.dictionary":
				return new Hashtable();
			case "properties":
			case "java.util.properties":
				return new Properties();
			default: {
				try {
					Class<?> clazz = null;
					try {
						clazz = this.parent.classLoader().loadClass(classNameString);
					} catch(RuntimeException | ClassNotFoundException ex1) {
						try { clazz = clazz!=null ? clazz : this.parent.classLoader().loadClass("java.lang."                   + classNameString); } catch(Exception | AssertionError ex2) { /** ignore */ }
						try { clazz = clazz!=null ? clazz : this.parent.classLoader().loadClass("java.util."                   + classNameString); } catch(Exception | AssertionError ex3) { /** ignore */ }
						try { clazz = clazz!=null ? clazz : this.parent.classLoader().loadClass("java.util.concurrent."        + classNameString); } catch(Exception | AssertionError ex4) { /** ignore */ }
						try { clazz = clazz!=null ? clazz : this.parent.classLoader().loadClass("java.util.concurrent.atomic." + classNameString); } catch(Exception | AssertionError ex5) { /** ignore */ }
						if(clazz==null) {
							throw ex1;
						}
					}
					final Object result = clazz.getDeclaredConstructor().newInstance();
					return result;
				} catch(ClassNotFoundException | NoSuchMethodException ex) {
					throw new IllegalArgumentException(ex);
				} catch(InvocationTargetException | InstantiationException | IllegalAccessException ex) {
					throw new UnsupportedOperationException(ex);
				}
			}
		}
	}

	public Icon loadIcon(Object resource) {
		final byte[] bytes = this.load(resource);
		final ImageIcon icon = new ImageIcon(bytes);
		return icon;
	}

	public byte[] load(Object resource) {
		if(resource==null) {
			throw new NullPointerException("Resource must not be null.");
		}
		InputStream in = null;
		ByteArrayOutputStream out = null;
		try {
			if((in==null) && (resource instanceof InputStream)) {
				in = (InputStream) resource;
			}
			if((in==null) && (resource instanceof File)) {
				in = new FileInputStream((File) resource);
			}
			if((in==null) && (resource instanceof Path)) {
				in = Files.newInputStream((Path) resource);
			}
			if((in==null) && (resource instanceof URL)) {
				try { in = ((URL) resource).openStream(); } catch(Exception | AssertionError ex) { /** ignore */ }
			}
			if((in==null) && (resource instanceof URI)) {
				try { in = ((URI) resource).toURL().openStream(); } catch(Exception | AssertionError ex) { /** ignore */ }
			}
			//try to create stream from string
			if(in==null) {
				final String pathString = this.toString(resource);
				//try URL first
				if(in==null) {
					try {
						URL url = this.parent.url();
						if((url==null) && (this.url()!=null)) {
							url = new URL(this.url());;
						}
						if(url!=null) {
							final URL pathUrl = new URL(url, pathString);
							in = pathUrl.openStream();
						}
					} catch(Exception | AssertionError ex) {
						//ignore
					}
				}
				if(in==null) {
					try {
						final URL url = new URL(pathString);
						in = url.openStream();
					} catch(Exception | AssertionError ex) {
						//ignore
					}
				}
				//then try the file
				if(in==null) {
					File file = new File(this.parent.directory(), pathString);
					if(!file.exists()) {
						file = new File(pathString);
					}
					if(file.exists()) {
						if((!file.isFile()) || (file.isDirectory())) {
							throw new IllegalArgumentException("'" + pathString + "' is not a file.");
						}
						in = new FileInputStream(file);
					}
				}
			}
			//read input stream
			if(in!=null) {
				out = new ByteArrayOutputStream();
				final byte[] buffer = new byte[8192];
				int len = in.read(buffer);
				while(len>0) {
					out.write(buffer, 0, len);
					len = in.read(buffer);
				}
				out.flush();
				return out.toByteArray();
			}
		} catch(final IOException ex) {
			throw new UnsupportedOperationException(ex);
		} finally {
			try { if(in!=null) { in.close(); } } catch(Exception | AssertionError ex) { /** ignore */ }
			try { if(out!=null) { out.close(); } } catch(Exception | AssertionError ex) { /** ignore */ }
		}

		throw new IllegalArgumentException("File '" + resource + "' not found.");
	}

	/**
	 * Converts the given script object into a java string.
	 * Null values be kept as null and will not be converted into the string "null".
	 * @param scriptString	Script object to convert into a string. Can be null.
	 * @return	The java string or null.
	 */
	public String toString(Object scriptString) {
		return scriptString!=null ? String.valueOf(scriptString) : null;
	}

	/**
	 * Converts the given script number object into a java Integer.
	 * @param scriptNumber	The script number to create an Integer from. Can be null.
	 * @return	The java Integer or null.
	 * @throws NumberFormatException	if the script number can not be converted into an Integer and is not null.
	 */
	public Integer toInteger(Object scriptNumber) throws NumberFormatException {
		if(scriptNumber==null) {
			return null;
		}
		if(scriptNumber instanceof Integer) {
			return (Integer) scriptNumber;
		}
		if(scriptNumber instanceof Number) {
			final int i = ((Number) scriptNumber).intValue();
			return Integer.valueOf(i);
		}
		final String string = this.toString(scriptNumber);
		if((string==null) || (string.isEmpty()) || ("NaN".equalsIgnoreCase(string)) || ("null".equalsIgnoreCase(string))) {
			return null;
		}
		return Integer.valueOf(string);
	}

	/**
	 * Converts the given script number object into a java Long.
	 * @param scriptNumber	The script number to create a Long from. Can be null.
	 * @return	The java Long or null.
	 * @throws NumberFormatException	if the script number can not be converted into a Long and is not null.
	 */
	public Long toLong(Object scriptNumber) throws NumberFormatException {
		if(scriptNumber==null) {
			return null;
		}
		if(scriptNumber instanceof Long) {
			return (Long) scriptNumber;
		}
		if(scriptNumber instanceof Number) {
			final long l = ((Number) scriptNumber).longValue();
			return Long.valueOf(l);
		}
		final String string = this.toString(scriptNumber);
		if((string==null) || (string.isEmpty()) || ("NaN".equalsIgnoreCase(string)) || ("null".equalsIgnoreCase(string))) {
			return null;
		}
		return Long.valueOf(string);
	}

	/**
	 * Converts the given script number object into a java Double.
	 * @param scriptNumber	The script number to create a Double from. Can be null.
	 * @return	The java Double or null.
	 * @throws NumberFormatException	if the script number can not be converted into a Double and is not null.
	 */
	public Double toDouble(Object scriptNumber) throws NumberFormatException {
		if(scriptNumber==null) {
			return null;
		}
		if(scriptNumber instanceof Double) {
			return (Double) scriptNumber;
		}
		if(scriptNumber instanceof Number) {
			final double d = ((Number) scriptNumber).doubleValue();
			return Double.valueOf(d);
		}
		final String string = this.toString(scriptNumber);
		if((string==null) || (string.isEmpty()) || ("NaN".equalsIgnoreCase(string)) || ("null".equalsIgnoreCase(string))) {
			return null;
		}
		return Double.valueOf(string);
	}

	public Boolean toBoolean(Object scriptBoolean) {
		if(scriptBoolean==null) {
			return null;
		}
		if(scriptBoolean instanceof Boolean) {
			return (Boolean) scriptBoolean;
		}
		String string = this.toString(scriptBoolean);
		if(string!=null) {
			string = string.trim().toLowerCase();
			final boolean b = "true".equals(string) || "yes".equals(string) || "y".equals(string) || "1".equals(string);
			return Boolean.valueOf(b);
		}
		return null;
	}

	public URL toURL(Object scriptUrl) {
		if(scriptUrl==null) {
			return null;
		}
		if(scriptUrl instanceof URL) {
			return (URL) scriptUrl;
		}
		try {
			if(scriptUrl instanceof File) {
				return ((File) scriptUrl).toURI().toURL();
			}
			if(scriptUrl instanceof URI) {
				return ((URI) scriptUrl).toURL();
			}
			final String string = this.toString(scriptUrl);
			if(string!=null) {
				return new URL(string);
			}
		} catch(final MalformedURLException ex) {
			throw new IllegalArgumentException(ex);
		}

		return null;
	}

	public Icon toIcon(Object scriptIcon) {
		if(scriptIcon==null) {
			return null;
		}
		if(scriptIcon instanceof Icon) {
			return (Icon) scriptIcon;
		}
		return this.loadIcon(scriptIcon);
	}

	public Map<?,?> toMap(Object scriptMap) {
		if(scriptMap==null) {
			return null;
		}
		if(scriptMap instanceof Map) {
			return (Map<?,?>) scriptMap;
		}
		//convert old dictionary objects (hashtables, properties ...)
		if(scriptMap instanceof Dictionary) {
			final HashMap<Object, Object> map = new HashMap<>();
			final Dictionary<?,?> dictionary = (Dictionary<?,?>) scriptMap;
			final Iterator<?> keys = dictionary.keys().asIterator();
			while(keys.hasNext()) {
				final Object key   = keys.next();
				final Object value = dictionary.get(key);
				map.put(key, value);
			}
			return map;
		}
		return null;
	}

	public List<Object> toList(Object scriptList) {
		if(scriptList==null) {
			return Collections.emptyList();
		}
		if(scriptList instanceof Collection) {
			return new ArrayList<>((Collection<?>) scriptList);
		}
		if(scriptList.getClass().isArray()) {
			final Object[] array = (Object[]) scriptList;
			return new ArrayList<>(Arrays.asList(array));
		}
		final ArrayList<Object> list = new ArrayList<>();
		list.add(scriptList);
		return list;
	}

	public PluginInfo toPluginInfo(Object scriptInfo) {
		if(scriptInfo==null) {
			return null;
		}
		if(scriptInfo instanceof PluginInfo) {
			return (PluginInfo) scriptInfo;
		}
		final Map<?,?> map = this.toMap(scriptInfo);
		if(map!=null) {
			final PluginInfoBuilder builder = new PluginInfoBuilder();
			for(final Map.Entry<?,?> entry:map.entrySet()) {
				final String key   = this.toString(entry.getKey());
				final Object value = entry.getValue();
				switch(key!=null ? key.toLowerCase().trim() : "null") {
					case "name":        builder.name(this.toString(value));        break;
					case "description": builder.description(this.toString(value)); break;
					case "version":     builder.version(this.toString(value));     break;
					case "website":     builder.website(this.toURL(value));        break;
					case "vendor":      builder.vendor(this.toString(value));      break;
					case "license":     builder.license(this.toString(value));     break;
					case "licenseurl":  builder.licenseURL(this.toURL(value));     break;
					case "icon":        builder.icon(this.toIcon(value));          break;
					default:
						this.out.println("Ignoring unsupported " + PluginInfo.class.getSimpleName() + " key '" + key + "' with value '" + value + "'.");
						break;
				}
			}
			return builder.build();
		}
		this.err.println("Unable to convert to " + PluginInfo.class.getSimpleName() + ": " + scriptInfo.getClass() + ", " + scriptInfo);
		return null;
	}

	public PatternParameter<String> newPatternStringParameter(final Object key, final Object title) {
		return PatternParameter.forString(this.toString(key), this.toString(title));
	}

	public PatternParameter<Integer> newPatternIntegerParameter(final Object key, final Object title, final Object min, final Object max) {
		return PatternParameter.forInteger(this.toString(key), this.toString(title), this.toInteger(min), this.toInteger(max));
	}

	public PatternParameter<Integer> newPatternIntegerParameter(final Object key, final Object title) {
		return PatternParameter.forInteger(this.toString(key), this.toString(title));
	}

	public PatternParameter<Boolean> newPatternBooleanParameter(final Object key, final Object title) {
		return PatternParameter.forBoolean(this.toString(key), this.toString(title));
	}

	public PatternParameter<Object> newPatternEnumParameter(final Object key, final Object title, final Object values) {
		return PatternParameter.forEnum(this.toString(key), this.toString(title), this.toList(values));
	}

	public PatternMatch newPatternMatch(Object scriptIndex, Object scriptLength) {
		final Integer index  = this.toInteger(scriptIndex);
		final Integer length = this.toInteger(scriptLength);
		if(index==null) {
			throw new NullPointerException("Index must not be null.");
		}
		if(length==null) {
			throw new NullPointerException("Length must not be null.");
		}
		return (index.intValue()>=0) && (length.intValue()>0)
			? new PatternMatch(index.intValue(), length.intValue())
			: null;
	}

	public PatternMatch toPatternMatch(Object scriptObject) {
		if(scriptObject==null) {
			return null;
		}
		if(scriptObject instanceof PatternMatch) {
			return (PatternMatch) scriptObject;
		}
		final Map<?,?> map = this.toMap(scriptObject);
		if(map!=null) {
			Integer index  = null;
			Integer length = null;
			for(final Map.Entry<?,?> entry:map.entrySet()) {
				final String key = this.toString(entry.getKey());
				switch(key!=null ? key.toLowerCase().trim() : "null") {
					case "index":
						index = this.toInteger(entry.getValue());
						break;
					case "length":
						length = this.toInteger(entry.getValue());
						break;
					case "text":
						//ignore
						break;
					default:
						throw new IllegalArgumentException("Unsupported pattern match parameter: " + key);
				}
			}
			return (index!=null) || (length!=null) ? this.newPatternMatch(index, length) : null;
		}

		throw new IllegalArgumentException("Unable create pattern match from '" + scriptObject + "'.");
	}

	public PrintStream toPrintStream(Object scriptObject) throws IOException {
		if(scriptObject==null) {
			throw new NullPointerException("Unable to create PrintStream for null.");
		}
		if(scriptObject instanceof PrintStream) {
			return (PrintStream) scriptObject;
		}
		int bufferSize = 32768;
		if(scriptObject instanceof OutputStream) {
			OutputStream out = (OutputStream)  scriptObject;
			if(!(out instanceof BufferedOutputStream)) {
				out = new BufferedOutputStream(out, bufferSize);
				this.registerForAutoClose(out);
			}
			final PrintStream ps = new PrintStream(out);
			this.registerForAutoClose(ps);
			return ps;
		}
		if(scriptObject instanceof File) {
			final OutputStream fout = new FileOutputStream((File) scriptObject);
			this.registerForAutoClose(fout);
			final BufferedOutputStream bout = new BufferedOutputStream(fout, bufferSize);
			this.registerForAutoClose(bout);
			final PrintStream ps = new PrintStream(bout);
			this.registerForAutoClose(ps);
			return ps;
		}
		if(scriptObject instanceof Path) {
			OutputStream out = Files.newOutputStream((Path) scriptObject);
			this.registerForAutoClose(out);
			if(!(out instanceof BufferedOutputStream)) {
				out = new BufferedOutputStream(out, bufferSize);
				this.registerForAutoClose(out);
			}
			final PrintStream ps = new PrintStream(out);
			this.registerForAutoClose(ps);
			return ps;
		}
		if(scriptObject instanceof String) {
			final OutputStream fout = new FileOutputStream(new File((String) scriptObject));
			this.registerForAutoClose(fout);
			final BufferedOutputStream bout = new BufferedOutputStream(fout, bufferSize);
			this.registerForAutoClose(bout);
			final PrintStream ps = new PrintStream(bout);
			this.registerForAutoClose(ps);
		}
		throw new IOException("Creating PrintStream for '" + scriptObject + "' failed.");
	}

}
