package com.mefir.log.o.mat.plugin.script;

public enum ScriptFunction {

	SELF_TEST("self_test"),
	TYPE("plugin_type"),
	INIT("plugin_init"),

	INFO("plugin_info"),
	INFO_FALLBACK_1("plugin_title"),
	INFO_FALLBACK_2("plugin_name"),
	INFO_FALLBACK_3("title"),
	INFO_FALLBACK_4("name"),

	PATTERN_MATCHER_PARAMETERS("pattern_matcher_parameters"),
	PATTERN_MATCHER_COMPILE("pattern_matcher_compile"),
	PATTERN_MATCHER_TEST("pattern_matcher_test"),
	PATTERN_MATCHER_NEXT("pattern_matcher_next"),

	LOG_ACTION_MATCHES("log_action_matches"),
	LOG_ACTION_RUN("log_action_run");

	private final String functionName;

	private ScriptFunction(String functionName) {
		this.functionName = functionName;
	}

	public String functionName() {
		return this.functionName;
	}

}
