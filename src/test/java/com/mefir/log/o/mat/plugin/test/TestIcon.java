package com.mefir.log.o.mat.plugin.test;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

class TestIcon implements Icon {

	@Override
	public void paintIcon(final Component c, final Graphics g, final int x, final int y) {
		g.setColor(Color.WHITE);
		g.fillRect(x, y, this.getIconWidth(), this.getIconHeight());

		g.setColor(Color.BLACK);
		g.drawLine(x, y, x + this.getIconWidth()-1, y + this.getIconHeight()-1);
		g.drawLine(x, y + this.getIconHeight()-1, x + this.getIconWidth()-1, y);
	}

	@Override
	public int getIconWidth() {
		return 64;
	}

	@Override
	public int getIconHeight() {
		return 128;
	}
}
