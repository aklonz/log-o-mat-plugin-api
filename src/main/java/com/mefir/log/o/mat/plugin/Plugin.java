package com.mefir.log.o.mat.plugin;

/**
 *
 * @param <PLUGIN>	self type of the plugin to prevent the user implement multiple plugin type in one class
 */
public interface Plugin<PLUGIN extends Plugin<PLUGIN>> {

	/**
	 * Provides optional information about the plugin.
	 * <br />
	 * <b>The resulting info object should not be depended on {@link #init(PluginContext)}.</b>
	 * This can be called by the main application even if {@link #init(PluginContext)}
	 * was not call before or reported a failure.
	 * @return optional information about the plugin
	 */
	PluginInfo info();

	/**
	 * Will be called by the main application to initialize the plugin.
	 * @param context	{@link PluginContext} provided by the main application.
	 * @return	<code>true</code> if the plugin could successfully be initialized; <code>false</code> otherwise
	 */
	boolean init(PluginContext context);

	/**
	 * Will be called by the main application when the plugin is no longer used.
	 */
	void dispose();

}
