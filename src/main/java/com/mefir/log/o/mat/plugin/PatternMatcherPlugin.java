package com.mefir.log.o.mat.plugin;

import java.util.List;

public interface PatternMatcherPlugin extends Plugin<PatternMatcherPlugin> {

	List<PatternMatcherFactory> getPatternMatcherFactories();

}
