package com.mefir.log.o.mat.plugin;

import javax.swing.Icon;
import java.util.List;
import java.util.Map;

public interface PatternMatcherFactory {

	/**
	 * @return	Unique key of this factory
	 */
	String key();

	/**
	 * @return	Title for this factory to show in the UI.
	 */
	String title();

	/**
	 * @return	optional icon of this factory to show in the UI.
	 */
	default Icon icon() {
		return null;
	}

	/**
	 * @return	List of parameters for this pattern factory.
	 */
	List<PatternParameter<?>> parameters();

	/**
	 * Creates a filter pattern for the given parameter values.
	 * @param values	Parameter values to create the filter pattern with.
	 * @return	The created filter pattern.
	 */
	PatternMatcher createPattern(Map<PatternParameter<?>, Object> values);

}
