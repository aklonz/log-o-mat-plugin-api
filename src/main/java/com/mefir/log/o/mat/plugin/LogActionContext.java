package com.mefir.log.o.mat.plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;

public interface LogActionContext extends Context {

	/**
	 * @return	<code>true</code> if the process job was cancelled by the main application
	 */
	boolean cancelled();


	/**
	 * Callback to update the progress.
	 * @param progress	current progress a double between 0 and 1.
	 * 					For negative values, the progress dialog will be disabled.
	 */
	void progress(double progress);


	/**
	 * @return	the log the log action is running on
	 */
	LogContext log();


	/**
	 * Invokes the {@link LogLineParser#matches(LogLineContext)} method of all
	 * available parsers and returns lazy results for all matching parsers.
	 * @param line	Line to get parser results for.
	 * @return	List of lazy parser results for all matching parsers.
	 */
	List<LazyLogLineParserResult> parse(LogLineContext line);


	/**
	 * Opens the given file as new log with the given title.
	 * @param file	file to open
	 * @param title	title for the log
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(File file, String title) throws IOException;


	/**
	 * Opens the given file as new log.
	 * @param file	file to open
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(File file) throws IOException;


	/**
	 * Opens the given path as new log with the given title.
	 * @param path	path to open
	 * @param title	title for the log
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(Path path, String title) throws IOException;


	/**
	 * Opens the given path as new log.
	 * @param path	path to open
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(Path path) throws IOException;


	/**
	 * Opens the given URL as new log with the given title.
	 * @param url	URL to open
	 * @param title	title for the log
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(URL url, String title) throws IOException;


	/**
	 * Opens the given URL as new log.
	 * @param url	URL to open
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(URL url) throws IOException;


	/**
	 * Opens the given input stream as new log with the given title.
	 * @param in	input stream to open
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(InputStream in) throws IOException;


	/**
	 * Opens the given input stream as new log.
	 * @param in	input stream to open
	 * @param title	title for the log
	 * @return	{@link LogContext} of the new log.
	 */
	LogContext open(InputStream in, String title) throws IOException;


	/**
	 * Creates a new temporary file;
	 * @return	the created temporary file
	 */
	File createTemporaryFile() throws IOException;

}
