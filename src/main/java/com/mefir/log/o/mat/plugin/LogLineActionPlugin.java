package com.mefir.log.o.mat.plugin;

import java.util.List;

public interface LogLineActionPlugin extends Plugin<LogLineActionPlugin> {

	List<LogLineAction> getLogLineActions();

}
