package com.mefir.log.o.mat.plugin.test;

import com.mefir.log.o.mat.plugin.LazyLogLineParserResult;
import com.mefir.log.o.mat.plugin.LogAction;
import com.mefir.log.o.mat.plugin.LogActionContext;
import com.mefir.log.o.mat.plugin.LogActionPlugin;
import com.mefir.log.o.mat.plugin.LogLineContext;
import com.mefir.log.o.mat.plugin.LogSwingActionContext;
import com.mefir.log.o.mat.plugin.PluginContext;
import com.mefir.log.o.mat.plugin.PluginInfo;
import com.mefir.log.o.mat.plugin.PluginInfoBuilder;

import javax.swing.JLabel;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

public class TestLogActionPlugin implements LogActionPlugin {

	@Override
	public boolean init(PluginContext context) {
		// nothing to do
		return true;
	}

	@Override
	public void dispose() {
		// nothing to do
	}

	@Override
	public PluginInfo info() {
		return new PluginInfoBuilder()
				.name("test log action plugin")
				.version("1.0.0")
				.description("test plugin to test the plugin backend")
				.license("Apache 2.0")
				.licenseURL("https://www.apache.org/licenses/LICENSE-2.0.txt")
				.icon(new TestIcon())
				.build();
	}


	@Override
	public List<LogAction> getLogActions() {
		return List.of(new TestLogAction(), new TestSwingLogAction());
	}


	private static class TestLogAction implements LogAction {

		@Override
		public String title() {
			return "test log action";
		}

		@Override
		public boolean matches(final LogActionContext context) {
			return context!=null;
		}

		@Override
		public void run(final LogActionContext context) throws Exception {
			context.info("start...");
			File file = context.createTemporaryFile();
			try(FileOutputStream fout = new FileOutputStream(file); PrintStream pout = new PrintStream(fout)) {
				context.open(file, title() + " / " + context.log().logInfo().name());
				long nr = 0;
				long lines = context.log().logInfo().length();
				while((!context.cancelled()) && (nr<lines)) {
					LogLineContext line = context.log().line(nr++);
					String string = line.line().string();
					string = "line " + nr + " / " + lines + ": " + string;
					context.info(string);
					pout.println(string);

					List<LazyLogLineParserResult> parsed = context.parse(line);
					if(parsed!=null) {
						for(LazyLogLineParserResult result:parsed) {
							string = "\t\t-" + result.title() + ": " + result.parse();
							context.info(string);
							pout.println(string);
						}
					}

					Thread.sleep(100);
				}
			}

			context.info("...end");
		}
	}


	private static class TestSwingLogAction implements LogAction {

		@Override
		public String title() {
			return "test swing log action";
		}

		@Override
		public boolean matches(final LogActionContext context) {
			return context instanceof LogSwingActionContext;
		}

		@Override
		public void run(final LogActionContext context) {
			LogSwingActionContext swing = (LogSwingActionContext) context;

			JLabel label = new JLabel("Hello World");

			Runnable closedHandler = () -> {
				System.err.println("closed");
			};

			swing.addView(label, title(), closedHandler);
		}

	}

}
