package com.mefir.log.o.mat.plugin;

import java.time.ZonedDateTime;
import java.util.List;

public interface LogInfo {

	/**
	 * @return	the name of the log
	 */
	String name();

	/**
	 * @return	all files/inputs of the log
	 */
	List<FileInfo> files();

	/**
	 * @return the length of all files of the log in bytes.
	 */
	long byteLength();

	/**
	 * @return	number of read/processed bytes in this log
	 */
	long bytesRead();

	/**
	 * @return	reading progress of this log as double between 0 and 1.
	 */
	double progress();

	/**
	 * @return A {@link ZonedDateTime} value representing the date and time the file was modified.
	 */
	ZonedDateTime lastModified();

	/**
	 * @return	number of lines in this log
	 */
	long length();

}
