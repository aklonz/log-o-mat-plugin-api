package com.mefir.log.o.mat.plugin.script;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.mefir.log.o.mat.plugin.LogLineContext;
import com.mefir.log.o.mat.plugin.PatternMatch;
import com.mefir.log.o.mat.plugin.PatternMatcher;
import com.mefir.log.o.mat.plugin.PatternMatcherFactory;
import com.mefir.log.o.mat.plugin.PatternMatcherPlugin;
import com.mefir.log.o.mat.plugin.PatternParameter;
import com.mefir.log.o.mat.plugin.PluginContext;

class ScriptPluginContainer_PatternMatcherPlugin extends ScriptPluginContainer<ScriptPluginContainer_PatternMatcherPlugin> implements PatternMatcherPlugin, PatternMatcherFactory {

	private List<PatternParameter<?>> parameters;
	private List<PatternParameter<?>> nonNullParameters;

	public ScriptPluginContainer_PatternMatcherPlugin(ScriptPlugin<?> parent, Script script) {
		super(parent, script);
	}

	@Override
	public List<PatternMatcherFactory> getPatternMatcherFactories() {
		return Collections.singletonList(this);
	}

	@Override
	public boolean init(PluginContext context) {
		final boolean success = super.init(context);

		//read parameters if init succeeded
		if(success) {
			this.parameters = new ArrayList<>();
			this.nonNullParameters = new ArrayList<>();
			final List<Object> scriptParameters = this.script().utilities().toList(this.script().invokeFunction(ScriptFunction.PATTERN_MATCHER_PARAMETERS));
			int index = 0;
			for(final Object scriptParameter:scriptParameters) {
				final PatternParameter<?> parameter = this.toPatternParameter(index, scriptParameter);
				this.parameters.add(parameter);
				if(parameter!=null) {
					this.nonNullParameters.add(parameter);
				}
				index++;
			}
		}

		return success;
	}

	@Override
	public List<PatternParameter<?>> parameters() {
		return this.nonNullParameters;
	}

	@Override
	public PatternMatcher createPattern(final Map<PatternParameter<?>, Object> values) {
		final ArrayList<Object> valueList = new ArrayList<>();
		for(final PatternParameter<?> parameter:this.parameters) {
			final Object value = parameter!=null ? values.get(parameter) : null;
			//System.err.println("parameter " + parameter + " = " + value);
			valueList.add(value);
		}

		final Object compiledPattern = this.script().invokeFunction(ScriptFunction.PATTERN_MATCHER_COMPILE, valueList);
		"".replace(' ', '#');
		if(compiledPattern!=null) {
			final PatternMatcher matcher = new PatternMatcher() {
				@Override public boolean test(final LogLineContext lineContext) { return ScriptPluginContainer_PatternMatcherPlugin.this.test(compiledPattern, lineContext); }
				@Override public PatternMatch nextMatch(final LogLineContext lineContext, final int fromIndex) { return ScriptPluginContainer_PatternMatcherPlugin.this.nextMatch(compiledPattern, lineContext, fromIndex); }
			};
			return matcher;
		}

		return null;
	}

	private boolean test(Object compiledPattern, final LogLineContext lineContext) {
		final Object scriptResult = this.script().invokeFunction(ScriptFunction.PATTERN_MATCHER_TEST, compiledPattern, lineContext);
		if(scriptResult!=null) {
			final Boolean result = this.utilities().toBoolean(scriptResult);
			return Boolean.TRUE.equals(result);
		}
		return false;
	}


	private PatternMatch nextMatch(Object compiledPattern, final LogLineContext lineContext, final int fromIndex) {
		final Object scriptResult = this.script().invokeFunction(ScriptFunction.PATTERN_MATCHER_NEXT, compiledPattern, lineContext, fromIndex);
		final PatternMatch match = this.utilities().toPatternMatch(scriptResult);
		return match;
	}


	private PatternParameter<?> toPatternParameter(int index, Object scriptParameter) {
		if(scriptParameter==null) {
			return null;
		}
		if(scriptParameter instanceof PatternParameter) {
			return (PatternParameter<?>) scriptParameter;
		}
		final ScriptUtilities util = this.utilities();

		final List<Object> list = util.toList(scriptParameter);
		if((list!=null) && (list.size()>0)) {
			String type = util.toString(list.get(0));
			type = type!=null ? type.toLowerCase().trim() : "null";
			final String key = this.key() +  "-" + index + "-" + type;
			switch(type) {
				case "string":
					if(list.size()==2) {
						return util.newPatternStringParameter(key, list.get(1));
					}
					throw new IllegalArgumentException("Unsupported number of arguments for string parameter: " + list.size());
				case "int":
				case "integer":
					if(list.size()==2) {
						return util.newPatternIntegerParameter(key, list.get(1));
					}
					if(list.size()==4) {
						return util.newPatternIntegerParameter(key, list.get(1), list.get(2), list.get(3));
					}
					throw new IllegalArgumentException("Unsupported number of arguments for integer parameter: " + list.size());
				case "bool":
				case "boolean":
					if(list.size()==2) {
						return util.newPatternBooleanParameter(key, list.get(1));
					}
					throw new IllegalArgumentException("Unsupported number of arguments for boolean parameter: " + list.size());
				case "enum":
					if(list.size()==3) {
						return util.newPatternEnumParameter(key, list.get(1), list.get(2));
					}
					throw new IllegalArgumentException("Unsupported number of arguments for enum parameter: " + list.size());
				default:
					throw new IllegalArgumentException("Unsupported parameter type: '" + list.get(0) + "'");
			}
		}
		throw new IllegalArgumentException("Invalid pattern matcher parameter: " + scriptParameter);
	}

}
