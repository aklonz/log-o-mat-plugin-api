package com.mefir.log.o.mat.plugin.script;

import java.util.function.BiFunction;

import com.mefir.log.o.mat.plugin.LogActionPlugin;
import com.mefir.log.o.mat.plugin.PatternMatcherPlugin;
import com.mefir.log.o.mat.plugin.Plugin;

public enum ScriptPluginType {

	PatternMatcherPlugin(PatternMatcherPlugin.class, ScriptPluginContainer_PatternMatcherPlugin::new),
	LogActionPlugin(     LogActionPlugin.class,      ScriptPluginContainer_LogActionPlugin::new),
	;

	private final Class<? extends Plugin<?>> pluginClass;
	private final BiFunction<ScriptPlugin<?>, Script, ? extends Plugin<?>> factory;

	private <PLUGIN extends Plugin<?>> ScriptPluginType(Class<PLUGIN> pluginClass, BiFunction<ScriptPlugin<?>, Script, PLUGIN> factory) {
		this.pluginClass = pluginClass;
		this.factory = factory;
	}

	static ScriptPluginType forClass(Class<? extends Plugin<?>> pluginClass) {
		for(final ScriptPluginType type:ScriptPluginType.values()) {
			if(type.pluginClass==pluginClass) {
				return type;
			}
		}
		return null;
	}

	static ScriptPluginType forObject(Object object) {
		if(object!=null) {
			if(object instanceof ScriptPluginType) {
				return (ScriptPluginType) object;
			}
			final String string = String.valueOf(object).trim();
			for(final ScriptPluginType type:ScriptPluginType.values()) {
				if(string.equalsIgnoreCase(type.name())) {
					return type;
				}
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	<PLUGIN extends Plugin<?>> PLUGIN newInstance(Class<PLUGIN> pluginClass, ScriptPlugin<?> parent, Script script) {
		if(pluginClass==this.pluginClass) {
			final Plugin<?> plugin = this.factory.apply(parent, script);
			return (PLUGIN) plugin;
		}
		return null;
	}
}
