package com.mefir.log.o.mat.plugin;


import javax.swing.JComponent;

/**
 * Extended {@link LogActionContext} for swing applications.
 */
public interface LogSwingActionContext extends LogActionContext {

	/**
	 * Added the given {@link JComponent view} to the main application.
	 * @param view			swing component to add
	 * @param title			title for the view
	 * @param closedHandler	if not null, will be called when the view is closed by the main application
	 */
	void addView(JComponent view, String title, Runnable closedHandler);

	/**
	 * Added the given {@link JComponent view} to the main application.
	 * @param view			swing component to add
	 * @param closedHandler	if not null, will be called when the view is closed by the main application
	 */
	void addView(JComponent view, Runnable closedHandler);


	/**
	 * Added the given {@link JComponent view} to the main application.
	 * @param view			swing component to add
	 * @param title			title for the view
	 */
	void addView(JComponent view, String title);


	/**
	 * Added the given {@link JComponent view} to the main application.
	 * @param view			swing component to add
	 */
	void addView(JComponent view);

}

