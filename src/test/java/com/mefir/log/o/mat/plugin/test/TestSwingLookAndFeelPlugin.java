package com.mefir.log.o.mat.plugin.test;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;

import javax.swing.UIManager;

import com.mefir.log.o.mat.plugin.PluginContext;
import com.mefir.log.o.mat.plugin.PluginInfo;
import com.mefir.log.o.mat.plugin.PluginInfoBuilder;
import com.mefir.log.o.mat.plugin.SwingLookAndFeelContainer;
import com.mefir.log.o.mat.plugin.SwingLookAndFeelPlugin;

public class TestSwingLookAndFeelPlugin implements SwingLookAndFeelPlugin {

	private final SwingLookAndFeelContainer laf1 = new SwingLookAndFeelContainer() {

		@Override
		public String key() {
			return "test.laf.1-1";
		}

		@Override
		public String title() {
			return "Test 1-1 (Motif)";
		}

		@Override
		public void install() throws Exception {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
		}
	};

	private final SwingLookAndFeelContainer laf2 = new SwingLookAndFeelContainer() {

		@Override
		public String key() {
			return "test.laf.1-2";
		}

		@Override
		public String title() {
			return "Test 1-2 (System)";
		}

		@Override
		public void install() throws Exception {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
	};

	private PluginContext context;

	@Override
	public boolean init(PluginContext context) {
		this.context = context;
		context.info(">>> init <<<");

		context.error(">>> config test", context.config().parameter("string",    "String Parameter",    "Hello World"));
		context.error(">>> config test", context.config().parameter("boolean",   "Boolean Parameter",   true));
		context.error(">>> config test", context.config().parameter("color",     "Color Parameter",     Color.RED));
		context.error(">>> config test", context.config().parameter("integer-1", "Integer Parameter",   123));
		context.error(">>> config test", context.config().parameter("integer-2", "Integer Parameter 2", 10, 20, 15));

		return true;
	}

	@Override
	public void dispose() {
		this.context.info(">>> dispose <<<");
	}

	@Override
	public List<SwingLookAndFeelContainer> getLookAndFeels() {
		return Arrays.asList(this.laf1, this.laf2);
	}

	@Override
	public PluginInfo info() {
		return new PluginInfoBuilder()
				.name("test look and feel plugin")
				.version("0.0.1")
				.description("test plugin to test the plugin backend")
				.license("Apache 2.0")
				.licenseURL("https://www.apache.org/licenses/LICENSE-2.0.txt")
				.icon(new TestIcon())
				.build();
	}
}
