package com.mefir.log.o.mat.plugin.script;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;

import com.mefir.log.o.mat.plugin.PluginContext;
import com.mefir.log.o.mat.plugin.PluginInfo;
import com.mefir.log.o.mat.plugin.PluginInfoBuilder;

@SuppressWarnings({"rawtypes", "unchecked"})
class ScriptPluginContainer<SELF extends ScriptPluginContainer<SELF>> {

	private ScriptPlugin<?> parent;
	private Script script;
	private PluginContext context;
	private PluginInfo info;
	private String title;
	private String key;
	private boolean disposeScript = false;

	ScriptPluginContainer(ScriptPlugin<?> parent, Script script) {
		this.parent = parent;
		this.script = script;
	}

	ScriptPlugin<?> parent() {
		return this.parent;
	}

	Script script() {
		return this.script;
	}

	ScriptUtilities utilities() {
		return this.script.utilities();
	}

	PluginContext context() {
		return this.context;
	}


	public boolean init(PluginContext context) {
		this.context = context;

		final Object scriptInitResult = this.script.invokeFunction(ScriptFunction.INIT, context);
		final Boolean result =  this.script.utilities().toBoolean(scriptInitResult);

		//reset cached info in case the init() changed some details
		info = null;

		return Boolean.TRUE.equals(result);
	}

	public void dispose() {
		Script s = this.script;

		this.parent = null;
		this.script = null;
		this.context = null;

		if((s!=null) && (this.disposeScript)) {
			s.dispose();
		}
	}

	public PluginInfo info() {
		PluginInfo i = this.info;
		if(i==null) {
			try {
				final Object scriptObject = this.script.invokeFunction(ScriptFunction.INFO);
				i = this.utilities().toPluginInfo(scriptObject);
			} catch(Exception | AssertionError ex) {
				//ignore
			}
			if(i==null) {
				i = new PluginInfoBuilder().name(this.fallBackName()).build();
			}
			this.info = i;
		}
		return i;
	}

	private String fallBackName() {
		String name = null;
		if((name==null) || ("".equals(name))) {
			final File file = this.script.file();
			try { name = file!=null ? file.getName().trim() : null; } catch(Exception | AssertionError ex) { /** ignore */ }
		}
		if((name==null) || ("".equals(name))) {
			final URL url = this.script.url();
			try { name = url!=null ? url.getFile().trim() : null; } catch(Exception | AssertionError ex) { /** ignore */ }
		}
		if((name==null) || ("".equals(name))) {
			try { name = this.utilities().toString(this.script.invokeFunction(ScriptFunction.INFO_FALLBACK_1)).trim(); } catch(Exception | AssertionError ex) { /** ignore */ }
		}
		if((name==null) || ("".equals(name))) {
			try { name = this.utilities().toString(this.script.invokeFunction(ScriptFunction.INFO_FALLBACK_2)).trim(); } catch(Exception | AssertionError ex) { /** ignore */ }
		}
		if((name==null) || ("".equals(name))) {
			try { name = this.utilities().toString(this.script.invokeFunction(ScriptFunction.INFO_FALLBACK_3)).trim(); } catch(Exception | AssertionError ex) { /** ignore */ }
		}
		if((name==null) || ("".equals(name))) {
			try { name = this.utilities().toString(this.script.invokeFunction(ScriptFunction.INFO_FALLBACK_4)).trim(); } catch(Exception | AssertionError ex) { /** ignore */ }
		}
		if((name==null) || ("".equals(name))) {
			name = this.parent.getClass().getName() + "_" + (Integer.toHexString(this.hashCode()).toUpperCase());
		}
		return name;
	}

	public String title() {
		String t = this.title;
		if((t==null) || ("".equals(t))) {
			try { t = this.info().name().trim(); } catch(Exception | AssertionError ex) { /** ignore */ }
			if((t==null) || ("".equals(t))) {
				t = this.fallBackName();
			}
			this.title = t;
		}
		return t;
	}

	public String key() {
		String k = this.key;
		if(k==null) {
			String n = this.script.fileString();
			n = n!=null ? n.trim() : null;

			n = (n!=null) && (!"".equals(n)) ? n : this.script.urlString();
			n = n!=null ? n.trim() : null;

			n = (n!=null) && (!"".equals(n)) ? n : this.title();
			n = n!=null ? n.trim() : null;

			final char[] chars = ("script " + this.parent().getClass().getSimpleName() + " " + n).toLowerCase().toCharArray();
			final StringBuilder sb = new StringBuilder();
			for(final char c:chars) {
				if( ((c>='a') && (c<='z')) || ((c>='0') && (c<='9')) ) {
					sb.append(c);
				} else {
					sb.append(' ');
				}
			}
			k = sb.toString()
					.replaceAll(" +", " ")
					.trim()
					.replace(' ', '-');
			this.key = k;
		}
		return k;
	}


	public SELF clone() {
		Script scriptCopy = null;
		ScriptPluginContainer<?> pluginCopy = null;
		boolean success = false;
		try {
			scriptCopy = this.cloneScript();
			pluginCopy = this.newInstance(this.parent, scriptCopy);
			if(pluginCopy==null) {
				throw new NullPointerException();
			}
			if((pluginCopy.getClass()!=this.getClass())) {
				throw new ClassCastException("");
			}
			pluginCopy.disposeScript = true;

			//if this is initialized, the clone also has to be initialized
			if(this.context!=null) {
				boolean initSuccess = false;
				try {
					initSuccess = pluginCopy.init(this.context);
				} catch(Throwable th) {
					throw new UnsupportedOperationException("Unable to initialize cloned " + getClass().getSimpleName() + " instance.", th);
				}
				if(!initSuccess) {
					throw new IllegalArgumentException("Initializing cloned " + getClass().getSimpleName() + " instance failed.");
				}
			}

			success = true;
			return (SELF) pluginCopy;
		} catch(RuntimeException ex) {
			throw ex;
		} catch(Exception | AssertionError ex) {
			throw new UnsupportedOperationException(ex);
		} finally {
			if(!success) {
				try { if(pluginCopy!=null) { pluginCopy.dispose(); } } catch(Exception | AssertionError ex) { /** ignore */ }
				try { if(scriptCopy!=null) { scriptCopy.dispose(); } } catch(Exception | AssertionError ex) { /** ignore */ }
			}
		}
	}

	protected SELF newInstance(ScriptPlugin parent, Script script) {
		Object newInstance = null;
		try {
			Constructor constructor = this.getClass().getConstructor(ScriptPlugin.class, Script.class);
			newInstance = constructor.newInstance(parent, script);
			return (SELF) newInstance;
		} catch(Throwable th) {
			try { if(newInstance!=null) { ((ScriptPluginContainer<?>) newInstance).dispose(); } } catch(Throwable th2) { /** ignore */ }
			throw new UnsupportedOperationException("Unable to create a new instance of plugin " + this.getClass().getSimpleName() + ".", th);
		}
	}


	private Script cloneScript() throws Exception {
		Script original = this.script;
		Script scriptCopy = null;
		boolean success = false;
		try {
			//create a script copy
			scriptCopy = this.parent().loadScript(original.classLoader(), original.directory(), original.script());
			if((original.file()!=null) && (scriptCopy.file()==null)) {
				scriptCopy.file(original.file());
			}
			if((original.url()!=null) && (scriptCopy.url()==null)) {
				scriptCopy.url(original.url());
			}
			if((original.plugin()!=null) && (scriptCopy.plugin()==null)) {
				scriptCopy.plugin(original.plugin());
			}
			scriptCopy.utilities().out = original.utilities().out;
			scriptCopy.utilities().err = original.utilities().err;
			scriptCopy.init();

			if(scriptCopy.valid()) {
				success = true;
				return scriptCopy;
			}

			throw new InstantiationException();
		} catch(Throwable th) {
			throw new UnsupportedOperationException("Unable to create a cloned instance of " + this.getClass().getSimpleName() + "/" + script.getClass().getSimpleName() + " script.", th);
		} finally {
			if((scriptCopy!=null) && (!success)) {
				try { scriptCopy.dispose(); } catch(Exception | AssertionError ex) { /** ignore */ }
			}
		}
	}


}
