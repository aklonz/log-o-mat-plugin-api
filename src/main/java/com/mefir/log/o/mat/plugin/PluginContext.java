package com.mefir.log.o.mat.plugin;

import java.io.File;
import java.net.URL;

public interface PluginContext extends Context {

	/**
	 * @return	The directory this plugin was loaded from.
	 */
	File directory();


	/**
	 * @return	The {@link URL} this plugin was loaded from.
	 */
	URL url();


	/**
	 * @return	The configuration of this plugin.
	 */
	PluginConfig config();


	/**
	 * @return	Permanent storage for this plugin.
	 */
	PluginStorage storage();


	/**
	 * @return	information about the application, this plugin is running in.
	 */
	ApplicationInfo applicationInfo();

}
