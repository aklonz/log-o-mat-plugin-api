package com.mefir.log.o.mat.plugin;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.Icon;

/**
 * Builder to create {@link PluginInfo} instances.
 */
public final class PluginInfoBuilder {

	private String name;
	private String description;
	private URL website;
	private String vendor;
	private String version;
	private String license;
	private URL licenseURL;
	private Icon icon;

	/**
	 * Creates a {@link PluginInfo} instance with current values of this builder.
	 * @return	a {@link PluginInfo} instance with current values of this builder
	 */
	public PluginInfo build() {
		return new PluginInfoImpl(this);
	}

	/**
	 * Copies all values from the given {@link PluginInfo} object.
	 * @param info	{@link PluginInfo} object to copy the values from. If <code>null</code>, all values will be cleared.
	 * @return	this builder
	 */
	public PluginInfoBuilder set(PluginInfo info) {
		this.name        = info!=null ? info.name()        : null;
		this.description = info!=null ? info.description() : null;
		this.website     = info!=null ? info.website()     : null;
		this.vendor      = info!=null ? info.vendor()      : null;
		this.version     = info!=null ? info.version()     : null;
		this.license     = info!=null ? info.license()     : null;
		this.licenseURL  = info!=null ? info.licenseURL()  : null;
		this.icon        = info!=null ? info.icon()        : null;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#name()} value.
	 * @param name	plugin name to set
	 * @return	this builder
	 */
	public PluginInfoBuilder name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#description()} value.
	 * @param description	plugin description to set
	 * @return	this builder
	 */
	public PluginInfoBuilder description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#website()} value.
	 * @param url	website URL to set
	 * @return	this builder
	 */
	public PluginInfoBuilder website(URL url) {
		this.website = url;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#website()} value.
	 * @param url	website URL to set
	 * @return	this builder
	 */
	public PluginInfoBuilder website(String url) {
		try {
			return this.website(new URL(url));
		} catch(final MalformedURLException ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	/**
	 * Sets the {@link PluginInfo#vendor()} value.
	 * @param vendor	vendor to set
	 * @return	this builder
	 */
	public PluginInfoBuilder vendor(String vendor) {
		this.vendor = vendor;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#version()} value.
	 * @param version	version to set
	 * @return	this builder
	 */
	public PluginInfoBuilder version(String version) {
		this.version = version;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#license()} value.
	 * @param license	license name/description to set
	 * @return	this builder
	 */
	public PluginInfoBuilder license(String license) {
		this.license = license;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#licenseURL()} value.
	 * @param url	license URL to set
	 * @return	this builder
	 */
	public PluginInfoBuilder licenseURL(URL url) {
		this.licenseURL = url;
		return this;
	}

	/**
	 * Sets the {@link PluginInfo#licenseURL()} value.
	 * @param url	license URL to set
	 * @return	this builder
	 */
	public PluginInfoBuilder licenseURL(String url) {
		try {
			return this.licenseURL(new URL(url));
		} catch(final MalformedURLException ex) {
			throw new IllegalArgumentException(ex);
		}
	}

	/**
	 * Sets the {@link PluginInfo#icon()} value.
	 * @param icon	Icon to set
	 * @return	this builder
	 */
	public PluginInfoBuilder icon(Icon icon) {
		this.icon = icon;
		return this;
	}


	private static class PluginInfoImpl implements PluginInfo {
		private final String name;
		private final String description;
		private final URL website;
		private final String vendor;
		private final String version;
		private final String license;
		private final URL licenseURL;
		private final Icon icon;

		private PluginInfoImpl(PluginInfoBuilder builder) {
			this.name        = builder.name;
			this.description = builder.description;
			this.website     = builder.website;
			this.vendor      = builder.vendor;
			this.version     = builder.version;
			this.license     = builder.license;
			this.licenseURL  = builder.licenseURL;
			this.icon        = builder.icon;;
		}

		@Override
		public String name() {
			return this.name;
		}

		@Override
		public String description() {
			return this.description;
		}

		@Override
		public URL website() {
			return this.website;
		}

		@Override
		public String vendor() {
			return this.vendor;
		}

		@Override
		public String version() {
			return this.version;
		}

		@Override
		public String license() {
			return this.license;
		}

		@Override
		public URL licenseURL() {
			return this.licenseURL;
		}

		@Override
		public Icon icon() {
			return this.icon;
		}
	}
}
