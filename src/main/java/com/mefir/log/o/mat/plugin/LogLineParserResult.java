package com.mefir.log.o.mat.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Container for a parsed tree and additional information created by an {@link LogLineParser} instance.
 */
public final class LogLineParserResult {

	private final Object lock = new Object();
	private final List<LogLineParserNode> messages = new LinkedList<>();
	private final Set<Long> consumedLines = new LinkedHashSet<>();
	private final List<String> skippedData = new LinkedList<>();
	private LogLineParserColumn treeColumn = null;
	private LogLineParserColumn skippedDataColumn = null;

	/**
	 * Creates an {@link LogLineParserResult} for a parsed tree.
	 * @param message	The parsed message tree; must not be <code>null</code>.
	 * @throws NullPointerException	if the message is <code>null</null>.
	 */
	public LogLineParserResult(LogLineParserNode message) throws NullPointerException {
		this.addMessage(message);
	}

	/**
	 * Creates an {@link LogLineParserResult} for a parsed trees.
	 * @param messages	The parsed message tree; must not be <code>null</code> or empty.
	 * @throws NullPointerException	if the messages is is <code>null</null>.
	 */
	public LogLineParserResult(List<LogLineParserNode> messages) throws NullPointerException {
		if(messages==null) {
			throw new NullPointerException("Messages must not be null.");
		}
		if(messages.isEmpty()) {
			throw new IllegalArgumentException("Messages must not be empty.");
		}
		for(final LogLineParserNode m:messages) {
			this.addMessage(m);
		}
	}

	/**
	 * @return	the parsed messages
	 */
	public List<LogLineParserNode> messages() {
		synchronized(this.lock) {
			return new ArrayList<>(this.messages);
		}
	}

	/**
	 * Adds an additional message
	 * @param message	The parsed message tree; must not be <code>null</code>.
	 * @throws NullPointerException	if the message is null
	 */
	public void addMessage(LogLineParserNode message) throws NullPointerException {
		if(message==null) {
			throw new NullPointerException("The parsed message must not be null.");
		}
		synchronized(this.lock) {
			this.messages.add(message);
		}
	}

	/**
	 * @return	Numbers of all lines, consumed by {@link LogLineParser} instance.
	 * 			Can be empty if the information is not provided by the {@link LogLineParser} instance.
	 */
	public Collection<Long> consumedLines() {
		synchronized(this.lock) {
			return new ArrayList<>(this.consumedLines);
		}
	}

	/**
	 * Adds the line number of a consumed line.
	 * @param lineNumber	Number of the consumed line.
	 */
	public void addConsumedLine(long lineNumber) {
		synchronized(this.lock) {
			this.consumedLines.add(lineNumber);
		}
	}

	/**
	 * Adds a collection of consumed line numbers.
	 * @param lineNumbers	The consumed line numbers. <code>null</code> and empty values will be ignored.
	 */
	public void addConsumedLines(Collection<Long> lineNumbers) {
		if(lineNumbers!=null) {
			synchronized(this.lock) {
				for(final Long lineNumber:lineNumbers) {
					if(lineNumber!=null) {
						this.consumedLines.add(lineNumber);
					}
				}
			}
		}
	}

	/**
	 * @return	Optional column definition for the tree structure. Can be <code>null</code> if not supported by the {@link LogLineParser} instance.
	 */
	public LogLineParserColumn treeColumn() {
		return this.treeColumn;
	}

	/**
	 * Sets the optional column definition for the tree structure.
	 * @param treeColumn	Column definition for the tree structure. Can be <code>null</code> if not supported by the {@link LogLineParser} instance.
	 */
	public void treeColumn(LogLineParserColumn treeColumn) {
		this.treeColumn = treeColumn;
	}

	/**
	 * @return	List of strings of the consumed lines, which are not part of the parsed tree.
	 * 			Can be empty if not supported by the {@link LogLineParser} instance.
	 */
	public List<String> skippedData() {
		synchronized(this.lock) {
			return new ArrayList<>(this.skippedData);
		}
	}

	/**
	 * Adds the given string to the {@link LogLineParserResult#skippedData()} list.
	 * @param skippedData	Sub string of a consumed line, which is not part of the parsed tree.
	 */
	public void addSkippedData(String skippedData) {
		if((skippedData!=null) && (skippedData.length()>0)) {
			synchronized(this.lock) {
				this.skippedData.add(skippedData);
			}
		}
	}

	/**
	 * Adds the given strings to the {@link LogLineParserResult#skippedData()} list.
	 * @param skippedData	Sub strings of consumed lines, which are not part of the parsed tree.
	 */
	public void addSkippedData(List<String> skippedData) {
		if(skippedData!=null) {
			synchronized(this.lock) {
				for(final String s:skippedData) {
					if((s!=null) && (s.length()>0)) {
						this.skippedData.add(s);
					}
				}
			}
		}
	}

	/**
	 * @return	Optional column definition for the {@link #skippedData()} lines, which are not part of the parsed tree.
	 * Can be <code>null</code> if not supported by the {@link LogLineParser} instance.
	 */
	public LogLineParserColumn skippedDataColumn() {
		return this.skippedDataColumn;
	}

	/**
	 * Sets the optional column definition for the {@link #skippedData()} lines, which are not part of the parsed tree.
	 * @param skippedDataColumn	Column definition for the tree structure.
	 * Can be <code>null</code> if not supported by the {@link LogLineParser} instance.
	 */
	public void skippedDataColumn(LogLineParserColumn skippedDataColumn) {
		this.skippedDataColumn = skippedDataColumn;
	}

	@Override
	public String toString() {
		synchronized(this.lock) {
			return this.messages.toString();
		}
	}
}
