package com.mefir.log.o.mat.plugin.script;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.function.Consumer;

class ScriptPrintStream extends PrintStream {

	ScriptPrintStream(Consumer<String> logger) {
		super(new OutputStream() {
			private StringBuilder buffer = new StringBuilder();
			@Override
			public synchronized void write(final int b) throws IOException {
				if(b>=0) {
					final char c = (char) b;
					if((c=='\n') || (c=='\r')) {
						this.flush();
					} else {
						this.buffer.append(c);
					}
				}
			}
			@Override
			public synchronized void flush() {
				if(this.buffer.length()>0) {
					logger.accept(this.buffer.toString());
					this.buffer = new StringBuilder();
				}
			}
		}, false, StandardCharsets.ISO_8859_1);
	}

}
