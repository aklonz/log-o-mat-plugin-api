package com.mefir.log.o.mat.plugin;

/**
 * Information about the application, the plugin is running in.
 */
public interface ApplicationInfo {

	/**
	 * @return	The name of the main application.
	 */
	String name();


	/**
	 * @return	The version of the main application.
	 */
	String version();


}

