package com.mefir.log.o.mat.plugin.script;

import java.io.Closeable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.mefir.log.o.mat.plugin.LogAction;
import com.mefir.log.o.mat.plugin.LogActionContext;
import com.mefir.log.o.mat.plugin.LogActionPlugin;

class ScriptPluginContainer_LogActionPlugin extends ScriptPluginContainer<ScriptPluginContainer_LogActionPlugin> implements LogActionPlugin, LogAction {

	public ScriptPluginContainer_LogActionPlugin(ScriptPlugin<?> parent, Script script) {
		super(parent, script);
	}

	@Override
	public List<LogAction> getLogActions() {
		return Collections.singletonList(this);
	}

	@Override
	public boolean matches(final LogActionContext context) {
		final Object scriptResult = this.script().invokeFunction(ScriptFunction.LOG_ACTION_MATCHES, context);
		final Boolean result = this.script().utilities().toBoolean(scriptResult);
		return Boolean.TRUE.equals(result);
	}

	@Override
	public void run(final LogActionContext context) {
		//log actions can take some time
		//work with script/plugin copy so that invokeFunction does not block everything
		ScriptPluginContainer_LogActionPlugin pluginCopy = null;
		try {
			//create plugin copy
			pluginCopy = this.clone();
			pluginCopy.script().invokeFunction(ScriptFunction.LOG_ACTION_RUN, context);
		} finally {
			try { if(pluginCopy!=null) { pluginCopy.dispose(); } } catch(Exception | AssertionError ex) { /** ignore */ }
		}
	}
}
