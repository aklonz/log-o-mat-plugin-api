package com.mefir.log.o.mat.plugin;

import java.util.List;

public interface LogActionPlugin extends Plugin<LogActionPlugin> {

	List<LogAction> getLogActions();

}

