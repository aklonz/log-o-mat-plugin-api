package com.mefir.log.o.mat.plugin.script;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mefir.log.o.mat.plugin.Plugin;
import com.mefir.log.o.mat.plugin.PluginContext;
import com.mefir.log.o.mat.plugin.PluginLoaderLoadedPlugin;
import com.mefir.log.o.mat.plugin.PluginLoaderPlugin;

public abstract class ScriptPlugin<SCRIPT extends Script> implements PluginLoaderPlugin {

	private final Object lock = new Object();
	private PluginContext context;
	private HashMap<String, List<SCRIPT>> loadedScripts = new HashMap<>();
	private final Set<String> extensions = new LinkedHashSet<>();


	public ScriptPlugin(String fileExtension, String ... moreFileExtensions) {
		final LinkedList<String> list = new LinkedList<>();
		list.add(fileExtension);
		if(moreFileExtensions!=null) {
			list.addAll(Arrays.asList(moreFileExtensions));
		}
		while(list.size()>0) {
			String ext = list.removeFirst();
			if(ext!=null) {
				ext = ext.toLowerCase().trim();
				if(ext.length()>0) {
					ext = ext.startsWith(".") ? ext : ("." + ext);
					this.extensions.add(ext);
				}
			}
		}
		if(this.extensions.isEmpty()) {
			throw new IllegalArgumentException("At least one valid extension has to be provided.");
		}
	}

	/**
	 * @return	A script with the method {@link ScriptFunction#SELF_TEST} which does not have any parameters and returns the string 'Okay' when called.
	 */
	protected abstract String selfTestScript();

	protected abstract SCRIPT loadScript(ClassLoader classLoader, File directory, String script);


	@Override
	public boolean init(PluginContext context) {
		this.context = context;
		boolean success = false;
		SCRIPT script = null;

		try {
			//try to load and init self test script
			script = this.loadScript(Thread.currentThread().getContextClassLoader(), context.directory(), this.selfTestScript());
			//redirect out and err to plugin logger
			script.utilities().out = new ScriptPrintStream((string) -> context.info("self test", string));
			script.utilities().err = new ScriptPrintStream((string) -> context.error("self test", string));
			script.init();

			//check the return value of the selfTest function
			final String selfTestResult = String.valueOf(script.invokeFunction(ScriptFunction.SELF_TEST));
			if("Okay".equals(selfTestResult)) {
				context.info("Self test passed.");
				success = true;
			} else {
				context.warn("Unexpected self test result: " + (selfTestResult!=null ? "'" + selfTestResult + "'" : "null"));
				success = false;
			}
		} catch(final Throwable th) {
			context.error("Self test failed", th);
		} finally {
			try { if(script!=null) { script.dispose(); } } catch(final Throwable th) { /** ignore */ }
		}

		return success;
	}

	@Override
	public <PLUGIN extends Plugin<?>> List<PluginLoaderLoadedPlugin<PLUGIN>> loadPlugins(ClassLoader classLoader, final File directory, final Class<PLUGIN> pluginClass) {
		synchronized (this.lock) {
			final ScriptPluginType type = ScriptPluginType.forClass(pluginClass);

			if(type!=null) {
				File dir = directory!=null ? directory : this.context.directory();
				dir = dir!=null ? dir : new File(".");
				final List<SCRIPT> scripts = this.loadScripts(classLoader, dir);
				if(scripts!=null) {
					final List<PluginLoaderLoadedPlugin<PLUGIN>> plugins = new LinkedList<>();
					for(final SCRIPT script:scripts) {
						if(script!=null) {
							try {
								final Object scriptTypeObject = script.invokeFunction(ScriptFunction.TYPE);
								final ScriptPluginType scriptType = ScriptPluginType.forObject(scriptTypeObject);
								if(scriptType==type) {
									final PLUGIN plugin = type.newInstance(pluginClass, this, script);
									if(plugin!=null) {
										final File file = script.file();
										final URL url = script.url();
										script.plugin(plugin); //link script to plugin, so it will not be used by other plugins
										plugins.add(new PluginLoaderLoadedPlugin<>(plugin, file, url));
									}
								}
							} catch(Exception | AssertionError ex) {
								this.context.error("Creating " + pluginClass.getSimpleName() + " from script " + script.fileString() + " failed", ex);
							}
						}
					}
					return plugins;
				}
			}

			return Collections.emptyList();
		}
	}

	protected List<SCRIPT> loadScripts(ClassLoader classLoader, final File directory) {
		final File[] array = directory.listFiles();
		if(array!=null) {
			final Map<String, SCRIPT> map = new LinkedHashMap<>();
			for(final File file:array) {
				if(file!=null) {
					if(file.exists()) {
						if(file.isDirectory()) {
							final List<SCRIPT> list = this.loadScripts(classLoader, file);
							if(list!=null) {
								for(final SCRIPT script:list) {
									if(script!=null) {
										final String fileString =  script.utilities().file();
										if(fileString!=null) {
											final String key = fileString.toLowerCase().trim();
											if((fileString.length()>0) && (map.get(key)==null)) {
												map.put(key, script);
											}
										}
									}
								}
							}
						} else {
							try {
								if(this.matches(file)) {
									final SCRIPT script = this.loadScript(classLoader, directory, file);
									final String fileString =  script.utilities().file();
									if(fileString!=null) {
										final String key = fileString.toLowerCase().trim();
										if((fileString.length()>0) && (map.get(key)==null)) {
											script.init();
											map.put(key, script);
										}
									}
								}
							} catch (Exception | AssertionError ex) {
								this.context.error("Loading script file '" + file.getAbsolutePath() + "' failed", ex);
							}
						}
					}
				}
			}
			if(map.size()>0) {
				return new LinkedList<>(map.values());
			}
		}
		return Collections.emptyList();
	}

	private boolean matches(File file) {
		if(file!=null) {
			final String lowerName = file.getName().toLowerCase().trim();
			if(lowerName.contains("plugin")) {
				for(final String ext:this.extensions) {
					if(lowerName.endsWith(ext)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void dispose() {
		final HashMap<String, List<SCRIPT>> ls = this.loadedScripts;

		this.context = null;
		this.loadedScripts = null;

		if(ls!=null) {
			for(final List<SCRIPT> list:ls.values()) {
				if(list!=null) {
					for(final SCRIPT s:list) {
						try { s.dispose(); } catch(Exception | AssertionError ex) { /** ignore */ }
					}
				}
			}
		}
	}

	protected SCRIPT loadScript(ClassLoader classLoader, File directory, File scriptFile) throws IOException  {
		if(scriptFile==null) {
			throw new NullPointerException("Script file must not be null.");
		}
		final SCRIPT script = this.loadScript(classLoader, directory, scriptFile, null);
		return script;
	}

	protected SCRIPT loadScript(ClassLoader classLoader, File directory, URL scriptUrl) throws IOException  {
		if(scriptUrl==null) {
			throw new NullPointerException("Script URL must not be null.");
		}
		final SCRIPT script = this.loadScript(classLoader, directory, null, scriptUrl);
		return script;
	}

	private SCRIPT loadScript(ClassLoader classLoader, File directory, File scriptFile, URL scriptUrl) throws IOException  {
		final String name;
		final String key;
		if(scriptFile!=null) {
			String n = scriptFile.getAbsolutePath();
			try { n = scriptFile.getCanonicalPath(); } catch(Exception | AssertionError ex) { /** ignore */ }
			name = n;
			key = "file:" + name;
		} else if(scriptUrl!=null) {
			name = scriptUrl.toString();
			key = "url:" + name;
		} else {
			throw new NullPointerException("Either file or URL must be not null.");
		}

		final List<SCRIPT> scripts;
		synchronized (this.lock) {
			//reuse already loaded script.
			List<SCRIPT> list = this.loadedScripts.get(key);
			if(list!=null) {
				for(final SCRIPT s:list) {
					//use if same classloader and not already linked to loaded plugin
					if((s.classLoader()==classLoader) && (s.plugin()==null)) {
						return s;
					}
				}
			} else {
				list = new LinkedList<>();
				this.loadedScripts.put(key, list);
			}

			//load new script bytes
			byte[] bytes;
			if(scriptFile!=null) {
				final Path path = scriptFile.toPath();
				bytes = Files.readAllBytes(path);
			} else if(scriptUrl!=null) {
				try (InputStream in = scriptUrl.openStream(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
					final byte[] buffer = new byte[8192];
					int len = in.read(buffer);
					while(len>0) {
						out.write(buffer, 0, len);
						len = in.read(buffer);
					}
					out.flush();
					bytes = out.toByteArray();
				}
			} else {
				throw new NullPointerException();
			}

			//convert bytes to string
			final Charset[] charsets = {
					StandardCharsets.UTF_8,
					StandardCharsets.UTF_16,
//					StandardCharsets.UTF_16BE,
//					StandardCharsets.UTF_16LE,
					StandardCharsets.ISO_8859_1,
					StandardCharsets.US_ASCII
			};
			String script = null;
			for(final Charset cs:charsets) {
				if(cs!=null) {
					try {
						final String string = new String(bytes, cs);
						final byte[] bytes2 = string.getBytes(cs);
						if(Arrays.equals(bytes, bytes2)) {
							script = string;
							break;
						}
					} catch(Exception | AssertionError ex) {
						//ignore
					}
				}
			}

			if(script==null) {
				throw new IllegalArgumentException("Unsupported charset in script " + name + ".");
			}

			//create script from string
			final SCRIPT s = this.loadScript(classLoader, directory, script);

			//set file and URL (if available and not already done by string-based loadScript()
			if((scriptFile!=null) && (s.file()==null)) {
				s.file(scriptFile);
			}
			if((scriptUrl!=null) && (s.url()==null)) {
				s.url(scriptUrl);
			}

			//redirect out and err to plugin logger
			s.utilities().out = new ScriptPrintStream((string) -> this.context.info(name, string));
			s.utilities().err = new ScriptPrintStream((string) -> this.context.error(name, string));

			//store script in cache for later reuse
			if(s!=null)  {
				list.add(s);
			}
			return s;
		}
	}
}
