package com.mefir.log.o.mat.plugin;

public enum PatternParameterType {
	String,
	Integer,
	Boolean,
	Enum
}
