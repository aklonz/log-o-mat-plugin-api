package com.mefir.log.o.mat.plugin;

public interface Context {

	/**
	 * Logs a debug message for this context.
	 * @param topic		Optional topic for the message.
	 * @param message	Message to log.
	 */
	void debug(String topic, Object message);

	/**
	 * Logs a debug message for this context.
	 * @param message	Message to log.
	 */
	void debug(Object message);

	/**
	 * Logs a info message for this context.
	 * @param topic		Optional topic for the message.
	 * @param message	Message to log.
	 */
	void info(String topic, Object message);

	/**
	 * Logs a info message for this context.
	 * @param message	Message to log.
	 */
	void info(Object message);

	/**
	 * Logs a warning message for this context.
	 * @param topic		Optional topic for the message.
	 * @param message	Message to log.
	 */
	void warn(String topic, Object message);

	/**
	 * Logs a warning message for this context.
	 * @param message	Message to log.
	 */
	void warn(Object message);

	/**
	 * Logs a error message for this context.
	 * @param topic		Optional topic for the message.
	 * @param message	Message to log.
	 */
	void error(String topic, Object message);

	/**
	 * Logs a error message for this context.
	 * @param message	Message to log.
	 */
	void error(Object message);

}
