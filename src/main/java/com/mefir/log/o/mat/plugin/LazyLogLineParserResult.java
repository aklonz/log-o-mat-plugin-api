package com.mefir.log.o.mat.plugin;


/**
 * Lazy wrapper for {@link LogLineParserResult}.
 */
public interface LazyLogLineParserResult {

	/**
	 * @return	title of the corresponding {@link LogLineParser} implementation.
	 */
	String title();

	/**
	 * Invokes the parse action of the corresponding {@link LogLineParser} implementation and returns the result.
	 * @return	Result of the corresonding parse action.
	 * @throws Exception	if the parse action throws an exception
	 */
	LogLineParserResult parse() throws Exception;

}
