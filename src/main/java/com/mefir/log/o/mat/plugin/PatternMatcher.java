package com.mefir.log.o.mat.plugin;

import java.util.function.Predicate;

public interface PatternMatcher extends Predicate<LogLineContext> {

	@Override
	boolean test(LogLineContext lineContext);

	PatternMatch nextMatch(LogLineContext lineContext, int fromIndex);

}
