package com.mefir.log.o.mat.plugin;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

/**
 * Column definition for {@link LogLineParserNode} and {@link LogLineParserLeaf} columns.
 * <br />
 * Columns are identified with their object identity.
 * {@link #name()} and {@link #index()} are just hints to tell the application,
 * how column should be rendered.
 * <br />
 * To create proper tree tables, {@link LogLineParser}s should use static {@link LogLineParserColumn}
 * instances which can be shared by all {@link LogLineParserNode} and {@link LogLineParserLeaf},
 * created by the parser.
 */
public final class LogLineParserColumn implements Comparable<LogLineParserColumn> {

	private final static AtomicLong ID_COUNTER = new AtomicLong(0);
	private final static HashMap<Integer, LogLineParserColumn> DUMMY_COLUMNS = new HashMap<>();
	private final long id = LogLineParserColumn.ID_COUNTER.incrementAndGet();
	private final int hash = Long.hashCode(this.id);
	private Supplier<String> name;
	private boolean keyColumn = false;
	private final int index;


	/**
	 * Creates a column with the given name and index.
	 * @param name	Display name supplier of this column. The result of the supplier will not be cached. It will be invoked every time {@link #name()} is called.
	 * @param index	Not binding hint for the application at which column index this column should be placed.
	 * @param keyColumn
	 */
	public LogLineParserColumn(final Supplier<String> name, final int index) {
		this(index);
		this.name(name);
	}

	/**
	 * Creates a column with the given name and index.
	 * @param name	Display name of this column.
	 * @param index	Not binding hint for the application at which column index this column should be placed.
	 */
	public LogLineParserColumn(final String name, final int index) {
		this(index);
		this.name(name);
	}

	/**
	 * Creates a column with the given name and index.
	 * @param index	Not binding hint for the application at which column index this column should be placed.
	 */
	public LogLineParserColumn(final int index) {
		this.index = index;
	}

	/**
	 * Creates a column with the given name.
	 * @param name	Display name of this column.
	 */
	public LogLineParserColumn(final String name) {
		this(name, Integer.MAX_VALUE);
	}

	static LogLineParserColumn dummyColumn(int index) {
		final Integer key = Integer.valueOf(index);
		synchronized (LogLineParserColumn.DUMMY_COLUMNS) {
			LogLineParserColumn column = LogLineParserColumn.DUMMY_COLUMNS.get(key);
			if(column!=null) {
				column = new LogLineParserColumn(index);
				LogLineParserColumn.DUMMY_COLUMNS.put(key, column);
			}
			return column;
		}
	}

	/**
	 * @return	Display name of this column.
	 */
	public String name() {
		final Supplier<String> supplier = this.name;
		if(supplier!=null) {
			final String n = supplier.get();
			if(n!=null) {
				return n;
			}
		}
		return "";
	}

	/**
	 * Sets the display name supplier of this column.
	 * The result of the supplier will not be cached. It will be invoked every time {@link #name()} is called.
	 * @param name	The new display name of this column.
	 * @return	this
	 */
	public LogLineParserColumn name(Supplier<String> name) {
		this.name = name;
		return this;
	}

	/**
	 * Sets the display name of this column.
	 * @param name	The new display name of this column.
	 * @return	this
	 */
	public LogLineParserColumn name(String name) {
		return this.name(() -> name);
	}


	/**
	 * @return	<code>true</true> if this column should be treated as key instead of data
	 * 			when comparing/merging multiple data sets.
	 * 			<code>false</code> otherwise.
	 */
	public boolean keyColumn() {
		return this.keyColumn;
	}

	/**
	 * Sets the key flag for this column.
	 * @param keyColumn	code>true</true> if this column should be treated as key instead of data
	 * 					when comparing/merging multiple data sets.
	 * 					<code>false</code> otherwise.
	 * @return	this
	 */
	public LogLineParserColumn keyColumn(boolean keyColumn) {
		this.keyColumn = keyColumn;
		return this;
	}

	/**
	 * Not binding hint for the application at which column index this column should be placed.
	 * @return	The index hint of this column.
	 */
	public int index() {
		return this.index;
	}


	@Override
	public int hashCode() {
		return this.hash;
	}

	@Override
	public boolean equals(final Object other) {
		return other==this;
	}

	@Override
	public int compareTo(final LogLineParserColumn other) {
		int c = Integer.compare(this.index, other.index);
		if(c==0) {
			c = Long.compare(this.id, other.id);
		}
		return c;
	}

	@Override
	public String toString() {
		return (this.index!=Integer.MAX_VALUE ? String.valueOf(this.index) : "MAX") + "[" + this.name() + "]";
	}
}
