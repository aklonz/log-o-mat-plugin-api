package com.mefir.log.o.mat.plugin;

import javax.swing.Icon;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 *
 * List/extract logic for an archive type.
 *
 */
public interface ArchiveExtractor {

	/**
	 * @return	name/title of this {@link ArchiveExtractor} implementation.
	 */
	String title();

	/**
	 * @return	optional icon of this {@link ArchiveExtractor} implementation.
	 */
	default Icon icon() {
		return null;
	}


	/**
	 * @return	List of supported file extensions of this archive plugin.
	 * 			Lower/upper case and '.' before the extension will be ignored.,
	 */
	List<String> fileExtensions();

	/**
	 * Checks if the given file is a supported archive.
	 * @param archive	File with one of the specified {@link #fileExtensions()}.
	 * @return	true if the file is a supported archive.
	 */
	boolean isSupported(File archive);

	/**
	 * Lists the content of the given archive file.
	 * @param archive	An archive file with one of the specified {@link #fileExtensions()} and {@link #isSupported(File)} = true.
	 * @return	A list of all {@link ArchiveEntry} for the given archive file.
	 */
	List<ArchiveEntry> list(File archive);

	/**
	 * Extracts the given entry of the given archive to the target {@link OutputStream}.
	 * The method is expected to block until the file is completely extracted.
	 * @param archive	Archive to extract the entry from.
	 * @param entry		Archive entry to extract.
	 * @param target	{@link OutputStream} to extract the archive to.
	 * @throws IOException	on I/O problems when extracting the entry
	 */
	void extract(ArchiveExtractContext context, File archive, ArchiveEntry entry, OutputStream target) throws IOException;

}
