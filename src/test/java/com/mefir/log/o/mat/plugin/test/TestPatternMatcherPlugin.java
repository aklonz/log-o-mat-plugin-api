package com.mefir.log.o.mat.plugin.test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.mefir.log.o.mat.plugin.LogContext;
import com.mefir.log.o.mat.plugin.LogLine;
import com.mefir.log.o.mat.plugin.LogLineContext;
import com.mefir.log.o.mat.plugin.PatternMatch;
import com.mefir.log.o.mat.plugin.PatternMatcher;
import com.mefir.log.o.mat.plugin.PatternMatcherFactory;
import com.mefir.log.o.mat.plugin.PatternMatcherPlugin;
import com.mefir.log.o.mat.plugin.PatternParameter;
import com.mefir.log.o.mat.plugin.PluginContext;
import com.mefir.log.o.mat.plugin.PluginInfo;
import com.mefir.log.o.mat.plugin.PluginInfoBuilder;

public class TestPatternMatcherPlugin implements PatternMatcherPlugin {

	@Override
	public boolean init(PluginContext context) {
		// nothing to do
		return true;
	}

	@Override
	public void dispose() {
		// nothing to do
	}

	@Override
	public PluginInfo info() {
		return new PluginInfoBuilder()
				.name("test pattern matcher plugin")
				.version("1.0.0")
				.description("test plugin to test the plugin backend")
				.license("Apache 2.0")
				.licenseURL("https://www.apache.org/licenses/LICENSE-2.0.txt")
				.icon(new TestIcon())
				.build();
	}

	@Override
	public List<PatternMatcherFactory> getPatternMatcherFactories() {
		return Arrays.asList(
			new TestPatternMatcherFactory(),
			new TestOtherLineAccessMatcherFactory()
		);
	}

	private class TestPatternMatcherFactory implements PatternMatcherFactory {

		private final PatternParameter<?> pattern = PatternParameter.forString("pattern", "Pattern");

		@Override
		public String key() {
			return "test.pattern.matcher";
		}

		@Override
		public String title() {
			return "Test Contains";
		}

		@Override
		public List<PatternParameter<?>> parameters() {
			return (List<PatternParameter<?>>) (Object) Collections.singletonList(this.pattern);
		}

		@Override
		public PatternMatcher createPattern(Map<PatternParameter<?>, Object> values) {
			String patternString = (String) values.get(this.pattern);
			if(patternString!=null) {
				patternString = patternString.trim();
				if(patternString.length()>0) {
					return new TestPatternMatcher(patternString);
				}
			}
			return null;
		}
	}

	private class TestPatternMatcher implements PatternMatcher {

		private final String pattern;

		private TestPatternMatcher(String pattern) {
			this.pattern = pattern;
		}

		@Override
		public boolean test(LogLineContext lineContext) {
			final LogLine line = lineContext.line();
			final String string = line.string();
			return string!=null ? string.contains(this.pattern) : false;
		}

		@Override
		public PatternMatch nextMatch(LogLineContext lineContext, int fromIndex) {
			final LogLine line = lineContext.line();
			final String string = line.string();
			final int index = string!=null ? string.indexOf(this.pattern, fromIndex) : -1;
			return index>=0 ? new PatternMatch(index, this.pattern.length()) : null;
		}
	}



	private class TestOtherLineAccessMatcherFactory implements PatternMatcherFactory {

		@Override
		public String key() {
			return "test.line.access.matcher";
		}

		@Override
		public String title() {
			return "Test Line Access";
		}

		@Override
		public List<PatternParameter<?>> parameters() {
			return Collections.emptyList();
		}

		@Override
		public PatternMatcher createPattern(Map<PatternParameter<?>, Object> values) {
			return new TestOtherLineAccessMatcher();
		}
	}

	private class TestOtherLineAccessMatcher implements PatternMatcher {
		@Override
		public boolean test(LogLineContext line) {
			final long nr = line.line().lineNumber();
			final LogContext log = line.log();
			final LogLineContext line_minus_1 = log.line(nr-1);
			final LogLineContext line_plus_1  = log.line(nr+1);

			return (line_minus_1!=null) && (line_plus_1!=null);
		}

		@Override
		public PatternMatch nextMatch(LogLineContext lineContext, int fromIndex) {
			return null;
		}
	}



}
