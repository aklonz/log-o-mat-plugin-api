package com.mefir.log.o.mat.plugin;

import java.awt.Color;

public interface PluginConfig {

	/**
	 * Returns the specified String parameter.
	 * If the parameter does not exist, it will be created.
	 * <br />
	 * <b>This method can only be used during initialization.</b>
	 * When using this method outside {@link Plugin#init(PluginContext)}
	 * an {@link IllegalStateException} will be thrown.
	 * This restriction does not apply to the returned
	 * {@link PluginConfigParameter} object. That object can be used
	 * during the complete plugin life cycle.
	 * @param key		Unique key of the parameter.
	 * @param title		Title of the parameter shown in the UI.
	 * @param defaultValue	the default value for the parameter
	 * @return	the created parameter
	 * @throws IllegalStateException if called from outside the {@link Plugin#init(PluginContext)} method.
	 */
	PluginConfigParameter<String> parameter(String key, String title, String  defaultValue);


	/**
	 * Returns the specified Boolean parameter.
	 * If the parameter does not exist, it will be created.
	 * <br />
	 * <b>This method can only be used during initialization.</b>
	 * When using this method outside {@link Plugin#init(PluginContext)}
	 * an {@link IllegalStateException} will be thrown.
	 * This restriction does not apply to the returned
	 * {@link PluginConfigParameter} object. That object can be used
	 * during the complete plugin life cycle.
	 * @param key		Unique key of the parameter.
	 * @param title		Title of the parameter shown in the UI.
	 * @param defaultValue    the default value for the parameter
	 * @return	the created parameter
	 * @throws IllegalStateException if called from outside the {@link Plugin#init(PluginContext)} method.
	 */
	PluginConfigParameter<Boolean> parameter(String key, String title, Boolean defaultValue);


	/**
	 * Returns the specified Integer parameter.
	 * If the parameter does not exist, it will be created.
	 * <br />
	 * <b>This method can only be used during initialization.</b>
	 * When using this method outside {@link Plugin#init(PluginContext)}
	 * an {@link IllegalStateException} will be thrown.
	 * This restriction does not apply to the returned
	 * {@link PluginConfigParameter} object. That object can be used
	 * during the complete plugin life cycle.
	 * @param key		Unique key of the parameter.
	 * @param title		Title of the parameter shown in the UI.
	 * @param defaultValue    the default value for the parameter
	 * @return	the created parameter
	 * @throws IllegalStateException if called from outside the {@link Plugin#init(PluginContext)} method.
	 */
	PluginConfigParameter<Integer> parameter(String key, String title, Integer defaultValue);


	/**
	 * Returns the specified Integer parameter.
	 * If the parameter does not exist, it will be created.
	 * <br />
	 * <b>This method can only be used during initialization.</b>
	 * When using this method outside {@link Plugin#init(PluginContext)}
	 * an {@link IllegalStateException} will be thrown.
	 * This restriction does not apply to the returned
	 * {@link PluginConfigParameter} object. That object can be used
	 * during the complete plugin life cycle.
	 * @param key		Unique key of the parameter.
	 * @param title		Title of the parameter shown in the UI.
	 * @param minValue		the minimum value for the parameter
	 * @param maxValue		the maximum value for the parameter
	 * @param defaultValue    the default value for the parameter
	 * @return	the created parameter
	 * @throws IllegalStateException if called from outside the {@link Plugin#init(PluginContext)} method.
	 */
	PluginConfigParameter<Integer> parameter(String key, String title, int minValue, int maxValue, Integer defaultValue);


	/**
	 * Returns the specified Color parameter.
	 * If the parameter does not exist, it will be created.
	 * <br />
	 * <b>This method can only be used during initialization.</b>
	 * When using this method outside {@link Plugin#init(PluginContext)}
	 * an {@link IllegalStateException} will be thrown.
	 * This restriction does not apply to the returned
	 * {@link PluginConfigParameter} object. That object can be used
	 * during the complete plugin life cycle.
	 * @param key		Unique key of the parameter.
	 * @param title		Title of the parameter shown in the UI.
	 * @param defaultValue	the default value for the parameter
	 * @return	the created parameter
	 * @throws IllegalStateException if called from outside the {@link Plugin#init(PluginContext)} method.
	 */
	PluginConfigParameter<Color> parameter(String key, String title, Color   defaultValue);
}
