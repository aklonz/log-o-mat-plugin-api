package com.mefir.log.o.mat.plugin;

import javax.swing.Icon;

public interface LogLineParser {

	/**
	 * @return	title of this {@link LogLineParser} implementation.
	 */
	String title();

	/**
	 * @return	optional icon of this {@link LogLineParser} implementation.
	 */
	default Icon icon() {
		return null;
	}

	boolean matches(LogLineContext lineContext);

	LogLineParserResult parse(LogLineContext lineContext);

}
