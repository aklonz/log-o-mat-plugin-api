package com.mefir.log.o.mat.plugin;

/**
 * Storage for plugin that can store a string with up to {@link #maxSize()} chars.
 * The stored data is not volatile and can be used even after the main application was restarted.
 */
public interface PluginStorage {

	/**
	 * @return	Maximum allowed size for the string to store with {@link #set(String)};
	 */
	int maxSize();


	/**
	 * @return	The string previously stored with {@link #set(String)};
	 */
	String get();


	/**
	 * Stores the given string in the permanent storage of the main application.
	 * @param data	String to store.
	 * @throws IllegalArgumentException if the data exceeds the {@link #maxSize()} limit.
	 */
	void set(String data) throws IllegalArgumentException;


}
