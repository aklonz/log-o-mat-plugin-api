package com.mefir.log.o.mat.plugin;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Tree node created by a {@link LogLineParser}.
 * Can be either the root node of the tree or a sub node.
 */
public final class LogLineParserNode extends LogLineParserElement {

	private final LinkedList<LogLineParserElement> elements = new LinkedList<>();
	private final LinkedList<LogLineParserLeaf> leafs = new LinkedList<>();
	private final LinkedList<LogLineParserNode> nodes = new LinkedList<>();

	/**
	 * Creates an instance with the given name.
	 * @param name	Title of this node
	 */
	public LogLineParserNode(final String name) {
		super(name);
	}

	/**
	 * @return	List of all direct sub nodes and leafs of this node. Can be empty.
	 */
	public List<LogLineParserElement> elements() {
		synchronized (this.lock) {
			return new ArrayList<>(this.elements);
		}
	}

	/**
	 * @return	List of all direct sub nodes of this node. Can be empty.
	 */
	public List<LogLineParserNode> nodes() {
		synchronized (this.lock) {
			return new ArrayList<>(this.nodes);
		}
	}

	/**
	 * Adds the given sub node to this node
	 * @param node	Sub node to add. Must not be <code>null</code>.
	 * @throws NullPointerException	if the sub node is <code>null</code>
	 * @throws UnsupportedOperationException	if adding the sub node would violate the tree structure
	 */
	public void add(final LogLineParserNode node) throws NullPointerException, UnsupportedOperationException {
		if(node==null) {
			throw new NullPointerException("Node must not be null.");
		}
		synchronized (this.lock) {
			node.parent(this);
			this.elements.add(node);
			this.nodes.add(node);
		}
	}

	/**
	 * @return	List of all leafs of this node. Can be empty.
	 */
	public List<LogLineParserLeaf> leafs() {
		synchronized (this.lock) {
			return new ArrayList<>(this.leafs);
		}
	}

	/**
	 * Adds the given leaf to this node
	 * @param leaf	Leaf to add. Must not be <code>null</code>.
	 * @throws NullPointerException	if the leaf is <code>null</code>.
	 * @throws UnsupportedOperationException	if adding the leaf would violate the tree structure
	 */
	public void add(final LogLineParserLeaf leaf) throws NullPointerException, UnsupportedOperationException {
		if(leaf==null) {
			throw new NullPointerException("Leaf must not be null.");
		}
		synchronized (this.lock) {
			leaf.parent(this);
			this.elements.add(leaf);
			this.leafs.add(leaf);
		}
	}

	@Override
	public String toString() {
		final List<String> v = this.values();
		synchronized (this.lock) {
			return this.name() + " = {"
					+ (v.size()>0           ? "values = " + v                                                            : "")
					+ (this.leafs.size()>0  ? (v.size()>0 ? ", " : "")                        + "leafs "   + this.leafs  : "")
					+ (this.nodes.size()>0  ? (v.size()>0 || this.leafs.size()>0 ? ", " : "") + "nodes = " + this.nodes  : "")
					+ "}"
					;
		}
	}
}
