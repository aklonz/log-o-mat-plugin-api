package com.mefir.log.o.mat.plugin;

import java.time.ZonedDateTime;

/**
 *
 * Interface representing a file entry of an {@link ArchiveExtractor}.
 *
 */
public interface ArchiveEntry {

	/**
	 * @return A {@link ZonedDateTime} value representing the date and time the archived file was modified if available. <code>null</code> otherwise.
	 */
	ZonedDateTime lastModified();

	/**
	 * @return The original length of the archived file in bytes if available. <code>null</code> otherwise.
	 */
	Long byteLength();

	/**
	 * @return	The name of the archived file. The {@link #name()} must not be <code>null</code> or empty.
	 */
	String name();

}
