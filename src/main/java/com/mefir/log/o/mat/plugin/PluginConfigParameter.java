package com.mefir.log.o.mat.plugin;

public interface PluginConfigParameter<TYPE> {

	String key();

	String title();

	TYPE get();

	void set(TYPE value);

}
