package com.mefir.log.o.mat.plugin;

import java.util.List;

/**
 *
 * Plugin to extend the Log-o-Mat with more archive types.
 *
 */
public interface ArchivePlugin extends Plugin<ArchivePlugin> {

	/**
	 * @return	List of archive extractors to add to the Log-o-Mat.
	 */
	List<ArchiveExtractor> getArchiveExtractors();

}
