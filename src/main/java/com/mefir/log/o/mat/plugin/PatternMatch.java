package com.mefir.log.o.mat.plugin;

public class PatternMatch {

	private final int index;
	private final int length;

	public PatternMatch(final int index, final int length) {
		this.index = index;
		this.length = length;
	}

	@Override
	public String toString() {
		return "(" + this.index + ", " + this.length + ")";
	}

	public int index() {
		return this.index;
	}

	public int length() {
		return this.length;
	}

}
