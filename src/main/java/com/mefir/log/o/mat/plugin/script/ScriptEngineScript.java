package com.mefir.log.o.mat.plugin.script;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public abstract class ScriptEngineScript extends Script {

//	private final static ConcurrentHashMap<String, Boolean> COMPILABLE_ENGINE_BUT_NOT_INVOCABLE_COMPILED_SCRIPT = new ConcurrentHashMap<>();
	private Set<String> triedEngineNames = new HashSet<>();
	private ScriptEngineManager engineManager;
	private ScriptEngine engine;
	private Invocable invocable;
	private Bindings bindings;

	protected ScriptEngineScript(ClassLoader classLoader, File directory, String script) {
		super(classLoader, directory, script);
	}

	protected ScriptEngineScript(File directory, String script) {
		this(Thread.currentThread().getContextClassLoader(), directory, script);
	}

	protected abstract List<Function<ScriptEngineManager, ScriptEngine>> engineSuppliers();

	@Override
	protected void dispose() {
		try {
			super.dispose();
		} finally {
			this.triedEngineNames = null;
			this.engineManager = null;
			this.engine = null;
			this.invocable = null;
			this.bindings = null;
		}
	}

	@Override
	protected boolean internalInit() throws Exception {
		this.engineManager = new ScriptEngineManager(this.classLoader());

		//System.out.println(engineManager.getEngineFactories());

		Throwable engineException = null;
		for(final Function<ScriptEngineManager, ScriptEngine> engineSupplier:this.engineSuppliers()) {
			try {
				if(this.initEngine(engineSupplier)) {
					boolean success =
							   (this.engineManager!=null)
							&& (this.engine!=null)
							&& (this.invocable!=null)
							&& (this.bindings!=null);
					return success;
				}
			} catch(ScriptException | RuntimeException | AssertionError ex) {
				if(engineException==null) {
					engineException = ex;
				} else {
					engineException.addSuppressed(ex);
				}
			}
		}

		if(engineException!=null) {
			throw (engineException instanceof Exception) ? ((Exception) engineException) : new IOException(engineException);
		}

		return false;
	}

	private boolean initEngine(Function<ScriptEngineManager, ScriptEngine> supplier) throws ScriptException {
		if(supplier==null) {
			return false;
		}

		final ScriptEngine e = supplier.apply(this.engineManager);

		if(e==null) {
			return false;
		}

		//do nothing the engine was already tried to compile with before
		final String engineName = e.getFactory().getEngineName();
		if(!this.triedEngineNames.add(engineName.toLowerCase())) {
			return false;
		}

		//init bindings
		final Bindings b = e.getBindings(ScriptContext.ENGINE_SCOPE);
		String dir  = this.directoryString();
		String file = this.fileString();
		String url  = this.urlString();
		b.put("__DIR__",  dir!=null  ? dir  : "");
		b.put("__FILE__", file!=null ? file : "");
		b.put("__URL__",  url!=null  ? url  : "");
		b.put("util",     this.utilities());

		Invocable i = null;
/* compiled scripts seem never to be Invocable (tested with graal, nashorn, rhino)
so skip this part
		//try to compile the script
		if((i==null) && (e instanceof Compilable) && (!Boolean.TRUE.equals(COMPILABLE_ENGINE_BUT_NOT_INVOCABLE_COMPILED_SCRIPT.get(engineName)))) {
			final Compilable compiler = (Compilable) e;
			final CompiledScript compiledScript = compiler.compile(this.script);

			if(compiledScript instanceof Invocable) {
				i = (Invocable) compiledScript;
			} else {
				//remember that the compiled result is not invocable
				COMPILABLE_ENGINE_BUT_NOT_INVOCABLE_COMPILED_SCRIPT.put(engineName, Boolean.TRUE);
			}
		}
*/
		//if not compilable try to use engine directly
		if((i==null) && (e instanceof Invocable)) {
			e.eval(this.script());
			i = (Invocable) e;
		}

		//abort if no invocable is available
		if(i==null) {
			throw new IllegalArgumentException("Unable to compile engine '" + engineName + "' to " + Invocable.class.getName() + ".");
		}

		this.engine = e;
		this.invocable = i;
		this.bindings = b;

		return true;
	}

	protected Object internalInvokeFunction(ScriptFunction function, Object ... arguments) throws Exception {
		return this.invocable.invokeFunction(function.functionName(), arguments);
	}

}
