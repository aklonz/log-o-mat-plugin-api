package com.mefir.log.o.mat.plugin;

import java.net.URL;

import javax.swing.Icon;

/**
 * Optional information about the plugin.
 */
public interface PluginInfo {

	/**
	 * @return	The name of the plugin. If null, the name of the plugin class will be used.
	 */
	String name();

	/**
	 * @return	A short description of the plugin.
	 */
	String description();

	/**
	 * @return	URL of the plugin's website.
	 */
	URL website();

	/**
	 * @return	The vendor of the plugin.
	 */
	String vendor();

	/**
	 * @return	The version of the plugin.
	 */
	String version();

	/**
	 * @return	Name or short description of the license of the plugin.
	 */
	String license();

	/**
	 * @return	URL of the license described in {@link #license()};
	 */
	URL licenseURL();

	/**
	 * @return	Icon of the plugin.
	 */
	Icon icon();

}
