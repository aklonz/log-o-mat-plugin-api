package com.mefir.log.o.mat.plugin;

import java.io.IOException;

public interface LogContext extends Context {

	LogInfo logInfo();

	void reload() throws IOException;

	LogLineContext line(long lineNumber);

	/**
	 * Stores reusable plugin data for this log.
	 * The data will be kept in short time memory while the plugin is processing the log and while enough memory is available.
	 * Stored data can be accessed with {@link #get(Object)}
	 * @param key	unique key to identify the data
	 * @param data	data to store
	 */
	void put(Object key, Object data);

	/**
	 * Gets data, previously created by this plugin and stored with {@link #put(Object, Object)}.
	 * It is not guaranteed for previously created data to be still available.
	 * Data will only be kept in short time memory and only while the plugin is processing the log and while enough memory is available
	 * @param key	unique key to identify the data
	 * @return		the requested data if available; <code>null</code> otherwise
	 */
	Object get(Object key);

}
