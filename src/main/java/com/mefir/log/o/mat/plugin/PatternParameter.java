package com.mefir.log.o.mat.plugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PatternParameter<TYPE> {

	private final PatternParameterType type;
	private final String key;
	private final String title;
	private final List<TYPE> values;
	private final TYPE min;
	private final TYPE max;

	private PatternParameter(final String key, final PatternParameterType type, final String title, final List<TYPE> values, final TYPE min, final TYPE max) {
		if(key==null) {
			throw new NullPointerException("Key must not be null.");
		}
		if(type==null) {
			throw new NullPointerException("Type must not be null.");
		}
		this.type = type;
		this.key = key.trim().toLowerCase();
		if(this.key.isEmpty()) {
			throw new IllegalArgumentException("Key must not be empty.");
		}
		this.title = title!=null ? title.trim() : "";
		this.values = values!=null
				? Collections.unmodifiableList(new ArrayList<>(values))
				: null
				;
		this.min = min;
		this.max = max;
	}

	public static PatternParameter<String> forString(final String key, final String title) {
		return new PatternParameter<String>(key, PatternParameterType.String, title, null, null, null);
	}

	public static PatternParameter<Integer> forInteger(final String key, final String title, final Integer min, final Integer max) {
		if((min!=null) && (max!=null)) {
			if(min.intValue()>=max.intValue()) {
				throw new IllegalArgumentException("Min must not be greater or equal to max.");
			}
		}
		return new PatternParameter<Integer>(key, PatternParameterType.Integer, title, null, min, max);
	}

	public static PatternParameter<Integer> forInteger(final String key, final String title) {
		return PatternParameter.forInteger(key, title, null, null);
	}

	public static PatternParameter<Boolean> forBoolean(final String key, final String title) {
		return new PatternParameter<Boolean>(key, PatternParameterType.Boolean, title, null, null, null);
	}

	public static <ENUM extends Enum<?>> PatternParameter<ENUM> forEnum(final String key, final String title, final Class<ENUM> clazz) {
		final List<ENUM> values = Arrays.asList(clazz.getEnumConstants());
		return new PatternParameter<ENUM>(key, PatternParameterType.Enum, title, values, null, null);
	}

	public static <TYPE> PatternParameter<TYPE> forEnum(final String key, final String title, final List<TYPE> values) {
		return new PatternParameter<TYPE>(key, PatternParameterType.Enum, title, values, null, null);
	}

	public static <TYPE> PatternParameter<TYPE> forEnum(final String key, final String title, final TYPE[] values) {
		return new PatternParameter<TYPE>(key, PatternParameterType.Enum, title, Arrays.asList(values), null, null);
	}


	/**
	 * @return	Type of this parameter.
	 */
	public PatternParameterType type() {
		return this.type;
	}

	/**
	 * @return	Unique to identify this parameter.
	 */
	public String key() {
		return this.key;
	}

	/**
	 * @return	Title of this parameter to show in the UI.
	 */
	public String title() {
		return this.title;
	}

	/**
	 * @return	List of possible values. Only used if {@link #type()} is {@link PatternParameterType#Enum}.
	 */
	public List<TYPE> values() {
		return this.values;
	}

	/**
	 * @return	Minimum value for {@link PatternParameterType#Integer} based parameters.
	 */
	public TYPE minValue() {
		return this.min;
	}

	/**
	 * @return	Maximum value for {@link PatternParameterType#Integer} based parameters.
	 */
	public TYPE maxValue() {
		return this.max;
	}

	@Override
	public String toString() {
		return "[" + this.key() + ", " + this.type() + "]";
	}
}

